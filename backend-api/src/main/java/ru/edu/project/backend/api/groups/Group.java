package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
public class Group {

    /**
     * id группы.
     */
    private Long id;

    /**
     * Наименования группы.
     */
    private String title;
}
