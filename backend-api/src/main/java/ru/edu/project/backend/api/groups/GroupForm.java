package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class GroupForm {

    /**
     * Наименование группы.
     */
    private String title;

    /**
     * Список id студентов.
     */
    private List<Long> students;
}
