package ru.edu.project.backend.api.groups;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface GroupService {

    /**
     * Получение информации о группе.
     * @param id
     * @return информация о группе
     */
    Group getGroup(long id);

    /**
     * Получение групп, к которым принадлежит студент.
     *
     * @param id
     * @return список
     */
    List<Group> getGroupsByStudentId(long id);

    /**
     * Получение id студентов, которые принадлежат к группе.
     *
     * @param id
     * @return список
     */
    List<Long> getStudentsByGroupId(long id);

    /**
     * Получение всех групп.
     *
     * @return список
     */
    List<Group> getAllGroups();

    /**
     * Создание новой группы.
     *
     * @param groupForm
     * @return запись
     */
    @AcceptorArgument
    Group createGroup(GroupForm groupForm);
}
