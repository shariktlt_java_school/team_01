package ru.edu.project.backend.api.roles;

public enum EnumRoles {
    /**
     *
     */
    STUDENT(1L),
    /**
     *
     */
    TEACHER(2L),
    /**
     *
     */
    ADMINISTRATOR(3L);

    /**
     *
     */
    private Long code;

    /**
     *
     * @param codeEnum
     */
    EnumRoles(final Long codeEnum) {
        this.code = codeEnum;
    }

    /**
     *
     * @return код
     */
    public Long getCode() {
        return code;
    }
}
