package ru.edu.project.backend.api.roles;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
public class Role {

    /**
     * id роли.
     */
    private Long id;

    /**
     * Наименование роли.
     */
    private String name;
}
