package ru.edu.project.backend.api.roles;

import java.util.List;

public interface RoleService {

    /**
     * Получение информации о роли по ее коду.
     *
     * @param code
     * @return Role
     */
    Role getRole(EnumRoles code);

    /**
     * Получение всех ролей.
     *
     * @return список
     */
    List<Role> getRoles();

    /**
     * Получение роли пользователя.
     *
     * @param id
     * @return  Role
     */
    Role getRoleByUserId(long id);

    /**
     * Получение пользователей по роли.
     *
     * @param code
     * @return список
     */
    List<Long> getUsersByRole(EnumRoles code);

}
