package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
public class StudentProgress {

    /**
     * id студента.
     */
    private Long userId;

    /**
     * id задания.
     */
    private Long taskId;

    /**
     * Ответ студента на задание.
     */
    private String response;

    /**
     * Оценка задания.
     */
    private int assessment;

    /**
     * Было ли проверено задание.
     */
    private int verified;
}
