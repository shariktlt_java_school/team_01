package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
public class Task {

    /**
     * id задания.
     */
    private Long id;

    /**
     * Наименование задания.
     */
    private String name;

    /**
     * Текст задания.
     */
    private String description;
}
