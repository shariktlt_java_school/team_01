package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class TaskForm {

    /**
     * Наименование задания.
     */
    private String name;

    /**
     * Текст задания.
     */
    private String description;

    /**
     * Список айдишников занятий.
     */
    private List<Long> idsLessons;
}
