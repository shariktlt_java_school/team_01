package ru.edu.project.backend.api.tasks;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface TaskService {

    /**
     * Получение информации по заданию.
     *
     * @param id
     * @return задание
     */
    Task getTask(long id);

    /**
     * Получение списка занятий к заданию.
     * @param id
     * @return list
     */
    List<Long> getLessonsIdByTaskId(long id);

    /**
     * Получение заданий по списку заданий.
     *
     * @param ids
     * @return список
     */
    List<Task> getTasks(List<Long> ids);

    /**
     * Получение всех заданий.
     *
     * @return список
     */
    List<Task> getAllTasks();

    /**
     * Получение всех заданий по занятию.
     *
     * @param id
     * @return список
     */
    List<Task> getTasksByLessonId(long id);

    /**
     * Получение всех заданий по занятиям.
     *
     * @param ids
     * @return список
     */
    @AcceptorArgument
    List<Task> getTasksByLessonsId(List<Long> ids);

    /**
     * Получение id всех сданных заданий студентом.
     *
     * @param id
     * @return список
     */
    List<Long> getProgressTasksIdsByStudentId(long id);

    /**
     * Получение решения по id студента и id задания.
     *
     * @param studentId
     * @param taskId
     * @return прогресс студента
     */
    StudentProgress getProgress(long studentId, long taskId);

    /**
     * Получение всех решений студентом.
     * @param id
     * @return список
     */
    List<StudentProgress> getProgressByStudentId(long id);

    /**
     * Создание нового задания.
     *
     * @param taskForm
     * @return запись
     */
    @AcceptorArgument
    Task createTask(TaskForm taskForm);

    /**
     * Создание ответа студента.
     *
     * @param progress
     * @return progress
     */
    @AcceptorArgument
    StudentProgress createProgress(StudentProgress progress);

    /**
     * Редактирование ответа студента.
     *
     * @param progress
     * @return progress
     */
    @AcceptorArgument
    StudentProgress updateProgress(StudentProgress progress);
}
