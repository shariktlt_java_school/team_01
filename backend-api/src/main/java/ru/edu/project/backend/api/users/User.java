package ru.edu.project.backend.api.users;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@Jacksonized
public class User {

    /**
     * id пользователя.
     */
    private Long id;

    /**
     * Имя.
     */
    private String firstName;

    /**
     * Фамилия.
     */
    private String lastName;

    /**
     * Отчество.
     */
    private String patronymic;

    /**
     * День рождения.
     */
    private Timestamp birthday;

    /**
     * Логин.
     */
    private String login;

    /**
     * Пароль.
     */
    private String password;

}
