package ru.edu.project.backend.api.users;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.sql.Timestamp;

@Getter
@Builder
@Jacksonized
public class UserForm {

    /**
     * Id пользователя.
     */
    private Long id;

    /**
     * Имя.
     */
    private String firstName;

    /**
     * Фамилия.
     */
    private String lastName;

    /**
     * Отчество.
     */
    private String patronymic;

    /**
     * День рождения.
     */
    private Timestamp birthDay;

    /**
     * Логин.
     */
    private String login;

    /**
     * Пароль.
     */
    private String password;

    /**
     *
     */
    private Long role;
}
