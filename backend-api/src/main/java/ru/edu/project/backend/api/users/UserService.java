package ru.edu.project.backend.api.users;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface UserService {

    /**
     * Регистрация пользователя.
     *
     * @param userForm
     * @return id
     */
    @AcceptorArgument
    Long register(UserForm userForm);

    /**
     * Получение данных о пользователе.
     *
     * @param login
     * @return username
     */
    User loadUserByLogin(String login);

    /**
     * Возвращает данные о пользователе.
     *
     * @param id
     * @return пользователь
     */
    User getUser(long id);

    /**
     * Возвращает список пользователей по списку id.
     *
     * @param ids
     * @return список
     */
    @AcceptorArgument
    List<User> getUsers(List<Long> ids);

    /**
     * Регистрация нового пользователя.
     *
     * @param userForm
     * @return запись
     */
    @AcceptorArgument
    User createUser(UserForm userForm);
}
