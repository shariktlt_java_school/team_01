package ru.edu.project.backend.stub.groups;

import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryStubGroupService implements GroupService {
    /**
     *
     */
    public static final long KEY = 4L;

    /**
     * Локальное хранилище отношения id студента к группам в RAM.
     */
    private Map<Long, List<Group>> db = new ConcurrentHashMap<>();

    /**
     * Локальное хранилище групп в RAM.
     */
    private List<Group> groups;

    /**
     * Конструктор.
     */
    public InMemoryStubGroupService() {
        groups = Arrays.stream(GroupsEnum.values())
                .map(GroupsEnum::getGroup)
                .collect(Collectors.toList());

        db.put(1L, groups);
        db.put(KEY, groups);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Group getGroup(final long id) {
        Map<Long, Group> groupMap = groups.stream().collect(Collectors.toMap(group -> group.getId(), group -> group));

        return groupMap.get(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Group> getGroupsByStudentId(final long id) {
        return db.get(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Long> getStudentsByGroupId(final long id) {
        List<Long> students = new ArrayList<>();
        db.forEach((key, groupsTmp) -> groupsTmp.forEach(group -> {
            if (group.getId().equals(id)) {
                students.add(key);
            }
        }));
        return students;
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Group> getAllGroups() {
        return groups;
    }

    /**
     * @inheritDoc
     */
    @Override
    public Group createGroup(final GroupForm groupForm) {
        Long id = Long.valueOf(groups.size() + 1);
        Group group = Group.builder()
                .id(id)
                .title(groupForm.getTitle())
                .build();
        groups.add(group);

        return group;
    }

    public enum GroupsEnum {

        /**
         * Загрушка группы 1.
         */
        GROUP_1(1L, "Группа А"),

        /**
         * Загрушка группы 2.
         */
        GROUP_2(2L, "Группа Б"),

        /**
         * Загрушка группы 3.
         */
        GROUP_3(3L, "Группа С"),

        /**
         * Загрушка группы 4.
         */
        GROUP_4(4L, "Группа Д");

        /**
         * Связанный объект.
         */
        private Group group;

        GroupsEnum(final Long id, final String title) {
            group = group.builder()
                    .id(id)
                    .title(title)
                    .build();
        }

        /**
         * Получение объекта.
         *
         * @return group
         */
        public Group getGroup() {
            return group;
        }
    }
}
