package ru.edu.project.backend.stub.lessons;

import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonForm;
import ru.edu.project.backend.api.lesson.LessonService;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryStubLessonService implements LessonService {
    /**
     *
     */
    public static final long KEY = 3L;
    /**
     * Локальное хранилище занятий в RAM.
     */
    private Map<Long, Lesson> lessons;

    /**
     * Локальное хранилище id групп к занятиям в RAM.
     */
    private Map<Long, List<Lesson>> groupLessons = new ConcurrentHashMap<>();

    /**
     * Локальное хранилище id учителей к занятиям в RAM.
     */
    private Map<Long, List<Lesson>> teacherLessons = new ConcurrentHashMap<>();

    /**
     *
     */
    public InMemoryStubLessonService() {
        lessons = Arrays.stream(LessonsEnum.values())
                .map(LessonsEnum::getLesson)
                .collect(Collectors.toMap(lesson -> lesson.getId(), lesson -> lesson));

        groupLessons.put(1L, lessons.values().stream().collect(Collectors.toList()));

        teacherLessons.put(KEY, lessons.values()
                .stream()
                .collect(Collectors.toList()));
        teacherLessons.put(2L, lessons.values()
                .stream()
                .filter(lesson -> {
                    if (lesson.getId() == 1) {
                        return true;
                    }
                    return false;
                }).collect(Collectors.toList()));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Lesson getLesson(final long id) {
        return lessons.get(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Lesson> getLessonsByGroupId(final long id) {
        return groupLessons.get(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Lesson> getLessonsByUserId(final long id) {
        return teacherLessons.get(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Lesson> getLessonsInfoByIds(final List<Long> ids) {
        return lessons.entrySet()
                .stream()
                .filter(entry -> ids.contains(entry.getKey()))
                .map(entry -> entry.getValue())
                .collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Long> getTeachersIdByLessonId(final long id) {
        List<Long> teachers = new ArrayList<>();

        teacherLessons.forEach((key, lessonsTmp) -> {
            lessonsTmp.forEach(lesson -> {
                if (lesson.getId().equals(id)) {
                    teachers.add(key);
                }
            });
        });
        return teachers;
    }

    /**
     *
     * @param lessonForm
     * @return lesson
     */
    @Override
    public Lesson createLesson(final LessonForm lessonForm) {
        return null;
    }

    /**
     * Получение списка групп, которые связаны с уроком.
     *
     * @param id
     * @return list
     */
    @Override
    public List<Long> getGroupsIdsByLessonId(final long id) {
        return null;
    }


    public enum LessonsEnum {

        /**
         * Загрушка занятия 1.
         */
        LESSON_1(1L, 100,
                Timestamp.from(Instant.now()),
                Timestamp.from(Instant.now().plus(1, ChronoUnit.HOURS))),

        /**
         * Загрушка занятия 2.
         */
        LESSON_2(2L, 200,
                Timestamp.from(Instant.now().plus(2, ChronoUnit.HOURS)),
                Timestamp.from(Instant.now().plus(3, ChronoUnit.HOURS))),

        /**
         * Загрушка занятия 3.
         */
        LESSON_3(3L, 200,
                Timestamp.from(Instant.now().plus(4, ChronoUnit.HOURS)),
                Timestamp.from(Instant.now().plus(5, ChronoUnit.HOURS))),

        /**
         * Загрушка занятия 4.
         */
        LESSON_4(4L, 300,
                Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS)),
                Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS).plus(1, ChronoUnit.HOURS))),
        /**
         * Загрушка занятия 5.
         */
        LESSON_5(5L, 200,
                Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS).plus(1, ChronoUnit.HOURS)),
                Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS).plus(2, ChronoUnit.HOURS))),

        /**
         * Загрушка занятия 6.
         */
        LESSON_6(6L, 400,
                Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS).plus(3, ChronoUnit.HOURS)),
                Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS).plus(4, ChronoUnit.HOURS)));

        /**
         * Связанный объект.
         */
        private Lesson lesson;

        LessonsEnum(final Long id, final int classRoom, final Timestamp startTime, final Timestamp endTime) {
            lesson = lesson.builder()
                    .id(id)
                    .classRoom(classRoom)
                    .startTime(startTime)
                    .endTime(endTime)
                    .build();
        }

        /**
         * Получение объекта.
         *
         * @return group
         */
        public Lesson getLesson() {
            return lesson;
        }
    }
}
