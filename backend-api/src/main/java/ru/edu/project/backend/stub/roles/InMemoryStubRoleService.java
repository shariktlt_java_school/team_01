package ru.edu.project.backend.stub.roles;

import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.api.roles.RoleService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryStubRoleService implements RoleService {
    /**
     *
     */
    public static final long KEY_4 = 4L;
    /**
     *
     */
    public static final long KEY_2 = 2L;
    /**
     *
     */
    public static final long KEY_3 = 3L;
    /**
     *
     */
    public static final long KEY_5 = 5L;
    /**
     * Локальное хранилище списка ролей в RAM.
     */
    private Map<Long, Role> roles;

    /**
     * Локальное хранилище отношения id пользователя к роли.
     */
    private Map<Long, Role> roleUser = new ConcurrentHashMap<>();

    /**
     * Конструктор.
     */
    public InMemoryStubRoleService() {
        roles = Arrays.stream(RolesEnum.values())
                .map(RolesEnum::getRole)
                .collect(Collectors.toMap(role -> role.getId(), role -> role));

        roleUser.put(1L, RolesEnum.ROLE_1.getRole());
        roleUser.put(KEY_4, RolesEnum.ROLE_1.getRole());

        roleUser.put(KEY_2, RolesEnum.ROLE_2.getRole());
        roleUser.put(KEY_3, RolesEnum.ROLE_2.getRole());

        roleUser.put(KEY_5, RolesEnum.ROLE_3.getRole());
    }

    /**
     * @inheritDoc
     */
    @Override
    public Role getRole(final EnumRoles code) {
        return roles.get(code);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Role> getRoles() {
        return roles.values().stream().collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    public Role getRoleByUserId(final long id) {
        return roleUser.get(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Long> getUsersByRole(final EnumRoles code) {
        List<Long> result = new ArrayList<>();
        roleUser.forEach((aLong, role) -> {
            if (role.getId().equals(code.getCode())) {
                result.add(aLong);
            }
        });

        return result;
    }

    public enum RolesEnum {

        /**
         * Заглушка роли 1.
         */
        ROLE_1(1L, "Студент"),

        /**
         * Заглушка роли 2.
         */
        ROLE_2(2L, "Преподаватель"),

        /**
         * Заглушка роли 3.
         */
        ROLE_3(3L, "Администратор");

        /**
         * Связанный объект.
         */
        private Role role;

        RolesEnum(final Long id, final String name) {
            role = role.builder()
                    .id(id)
                    .name(name)
                    .build();
        }

        /**
         * Получение объекта.
         *
         * @return group
         */
        public Role getRole() {
            return role;
        }
    }

}
