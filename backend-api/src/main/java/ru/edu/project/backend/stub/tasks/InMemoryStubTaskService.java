package ru.edu.project.backend.stub.tasks;

import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class InMemoryStubTaskService implements TaskService {
    /**
     *
     */
    public static final long KEY_3 = 3L;
    /**
     *
     */
    public static final long KEY_2 = 2L;
    /**
     * Локальное хранилище заданий в RAM.
     */
    private Map<Long, Task> tasks;

    /**
     * Локальное хранилище id занятий к заданиям в RAM.
     */
    private Map<Long, List<Task>> lessonTasks = new ConcurrentHashMap<>();

    /**
     * Локальное хранилище ответов на задания id студентов к заданиям в RAM.
     */
    private Map<Long, List<StudentProgress>> studentProgress;

    /**
     *
     */
    public InMemoryStubTaskService() {
        tasks = Arrays.stream(TasksEnum.values())
                .map(TasksEnum::getTask)
                .collect(Collectors.toMap(task -> task.getId(), task -> task));

        studentProgress = Arrays.stream(StudentProgressEnum.values())
                .map(StudentProgressEnum::getProgress)
                .collect(Collectors.toList()).stream().collect(groupingBy(progress -> progress.getUserId(), toList()));


        lessonTasks.put(1L, tasks.values().stream().collect(Collectors.toList()));
        lessonTasks.put(KEY_2, tasks.values().stream().filter(task -> {
            if (task.getId() != 2) {
                return true;
            }
            return false;
        }).collect(Collectors.toList()));
        lessonTasks.put(KEY_3, tasks.values().stream().filter(task -> {
            if (task.getId() == 2) {
                return true;
            }
            return false;
        }).collect(Collectors.toList()));

    }

    /**
     * @inheritDoc
     */
    @Override
    public Task getTask(final long id) {
        return tasks.get(id);
    }

    /**
     * Получение списка занятий к заданию.
     *
     * @param id
     * @return list
     */
    @Override
    public List<Long> getLessonsIdByTaskId(final long id) {
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Task> getTasks(final List<Long> ids) {
        return tasks.entrySet()
                .stream()
                .filter(entry -> ids.contains(entry.getKey()))
                .map(entry -> entry.getValue())
                .collect(Collectors.toList());
    }

    /**
     * Получение всех заданий.
     *
     * @return список
     */
    @Override
    public List<Task> getAllTasks() {
        return tasks.entrySet().stream().map(longTaskEntry -> longTaskEntry.getValue()).collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Task> getTasksByLessonId(final long id) {
        return lessonTasks.get(id);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Task> getTasksByLessonsId(final List<Long> ids) {
        List<Long> taskIds = lessonTasks.entrySet()
                .stream()
                .filter(entry -> ids.contains(entry.getKey()))
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());

        return getTasks(taskIds);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Long> getProgressTasksIdsByStudentId(final long id) {
        return studentProgress
                .get(id)
                .stream()
                .map(st -> st.getTaskId())
                .collect(toList());
    }

    /**
     * Получение решения по id студента и id задания.
     *
     * @param studentId
     * @param taskId
     * @return
     */
    @Override
    public StudentProgress getProgress(final long studentId, final long taskId) {
        List<StudentProgress> st = studentProgress.get(studentId);
        Optional<StudentProgress> result = st.stream().filter(progress -> {
            if (progress.getTaskId() == taskId) {
                return true;
            }
            return false;
        }).findFirst();
        return result.get();
    }

    /**
     * Получение всех решений студентом.
     *
     * @param id
     * @return
     */
    @Override
    public List<StudentProgress> getProgressByStudentId(final long id) {
        return studentProgress.get(id);
    }

    /**
     * @param taskForm
     * @return
     */
    @Override
    public Task createTask(final TaskForm taskForm) {
        return null;
    }

    /**
     * @param progress
     * @return progress
     */
    @Override
    public StudentProgress createProgress(final StudentProgress progress) {
        return null;
    }

    /**
     * @param progress
     * @return progress
     */
    @Override
    public StudentProgress updateProgress(final StudentProgress progress) {
        return null;
    }

    public enum TasksEnum {

        /**
         * Заглушка задания 1.
         */
        TASK_1(1L, "Задание 1", "Описание задания 1"),

        /**
         * Заглушка задания 2.
         */
        TASK_2(2L, "Задание 2", "Описание задания 2"),

        /**
         * Заглушка задания 3.
         */
        TASK_3(3L, "Задание 3", "Описание задания 3");

        /**
         * Связанный объект.
         */
        private Task task;

        TasksEnum(final Long id, final String title, final String description) {
            task = task.builder()
                    .id(id)
                    .name(title)
                    .description(description)
                    .build();
        }

        /**
         * Получение объекта.
         *
         * @return task
         */
        public Task getTask() {
            return task;
        }
    }

    public enum StudentProgressEnum {

        /**
         * Заглушка прогресса 1.
         */
        TASK_1(1L, 1L, "Ответ на задание 1", 0, 0),

        /**
         * Заглушка прогресса 2.
         */
        TASK_2(1L, 2L, "Ответ на задание 2", 5, 1);

        /**
         * Связанный объект.
         */
        private StudentProgress progress;

        StudentProgressEnum(final Long studentId, final Long taskId, final String response, final int assessment, final int verified) {
            progress = progress.builder()
                    .userId(studentId)
                    .taskId(taskId)
                    .response(response)
                    .assessment(assessment)
                    .verified(verified)
                    .build();
        }

        /**
         * Получение объекта.
         *
         * @return task
         */
        public StudentProgress getProgress() {
            return progress;
        }
    }
}
