package ru.edu.project.backend.stub.users;

import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserForm;
import ru.edu.project.backend.api.users.UserService;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryStubUserService implements UserService {

    /**
     * Локальное хранилище пользователей в RAM.
     */
    private Map<Long, User> db;

    /**
     *
     */
    public InMemoryStubUserService() {
        db = Arrays.stream(UsersEnum.values())
                .map(UsersEnum::getUser)
                .collect(Collectors.toMap(user -> user.getId(), user -> user));
    }

    /**
     *
     * @param userForm
     * @return
     */
    @Override
    public Long register(final UserForm userForm) {
        return null;
    }

    /**
     *
     * @param login
     * @return user
     */
    @Override
    public User loadUserByLogin(final String login) {
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public User getUser(final long id) {
        return db.get(id);
    }
    /**
     * @inheritDoc
     */
    @Override
    public List<User> getUsers(final List<Long> ids) {
        return db.entrySet()
                .stream()
                .filter(entry -> ids.contains(entry.getKey())) // key = 1L, value= User
                .map(entry -> entry.getValue())
                .collect(Collectors.toList());
    }

    /**
     * Регистрация нового пользователя.
     *
     * @param userForm
     * @return запись
     */
    @Override
    public User createUser(final UserForm userForm) {
        return null;
    }

    public enum UsersEnum {

        /**
         * Загрушка пользователя 1.
         */
        USER_1(1L, "Борис", "Студент", "Студентович", new Timestamp(new Date().getTime())),

        /**
         * Загрушка пользователя 2.
         */
        USER_2(2L, "Иван", "Учитель", "Учителевич", new Timestamp(new Date().getTime())),

        /**
         * Загрушка пользователя 3.
         */
        USER_3(3L, "Кирилл", "Учитель", "Учителевич", new Timestamp(new Date().getTime())),

        /**
         * Загрушка пользователя 4.
         */
        USER_4(4L, "Александр", "Студент", "Студентович", new Timestamp(new Date().getTime())),

        /**
         * Загрушка пользователя 4.
         */
        USER_5(5L, "Александр", "Администратор", "Администратович", new Timestamp(new Date().getTime()));

        /**
         * Связанный объект.
         */
        private User user;

        UsersEnum(final Long id,
                  final String firstName,
                  final String lastName,
                  final String patronymic,
                  final Timestamp birthday) {

            user = user.builder()
                    .id(id)
                    .firstName(firstName)
                    .lastName(lastName)
                    .patronymic(patronymic)
                    .birthday(birthday)
                    .build();
        }

        /**
         * Получение объекта.
         *
         * @return job
         */
        public User getUser() {
            return user;
        }
    }
}
