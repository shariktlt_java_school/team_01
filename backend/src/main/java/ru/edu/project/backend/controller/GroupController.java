package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.service.GroupServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController implements GroupService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private GroupServiceLayer delegate;

    /**
     * Получение информации о группе.
     *
     * @param id
     * @return информация о группе
     */
    @Override
    @GetMapping("/getGroup/{id}")
    public Group getGroup(@PathVariable("id") final long id) {
        return delegate.getGroup(id);
    }

    /**
     * Получение групп, к которым принадлежит студент.
     *
     * @param id
     * @return список
     */
    @Override
    @GetMapping("/getGroupsByStudentId/{id}")
    public List<Group> getGroupsByStudentId(@PathVariable("id") final long id) {
        return delegate.getGroupsByStudentId(id);
    }

    /**
     * Получение id студентов, которые принадлежат к группе.
     *
     * @param id
     * @return список
     */
    @Override
    @GetMapping("/getStudentsByGroupId/{id}")
    public List<Long> getStudentsByGroupId(@PathVariable("id") final long id) {
        return delegate.getStudentsByGroupId(id);
    }

    /**
     * Получение всех групп.
     *
     * @return список
     */
    @Override
    @GetMapping("/getAllGroups")
    public List<Group> getAllGroups() {
        return delegate.getAllGroups();
    }

    /**
     * Создание новой группы.
     *
     * @param groupForm
     * @return запись
     */
    @Override
    @PostMapping(value = "/createGroup", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Group createGroup(@RequestBody final GroupForm groupForm) {
        return delegate.createGroup(groupForm);
    }
}
