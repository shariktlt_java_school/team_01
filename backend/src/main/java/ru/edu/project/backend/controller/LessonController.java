package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonForm;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.service.LessonServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/lesson")
public class LessonController implements LessonService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private LessonServiceLayer delegate;

    /**
     * Получение информации о занятии.
     *
     * @param id
     * @return Lesson
     */
    @Override
    @GetMapping("/getLesson/{id}")
    public Lesson getLesson(@PathVariable("id") final long id) {
        return delegate.getLesson(id);
    }

    /**
     * Получение всех занятий по группе.
     *
     * @param id
     * @return информация о группе
     */
    @Override
    @GetMapping("/getLessonsByGroupId/{id}")
    public List<Lesson> getLessonsByGroupId(@PathVariable("id") final long id) {
        return delegate.getLessonsByGroupId(id);
    }

    /**
     * Получение занятий по id пользователя.
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/getLessonsByUserId/{id}")
    public List<Lesson> getLessonsByUserId(@PathVariable("id") final long id) {
        return delegate.getLessonsByUserId(id);
    }

    /**
     * Получение информации по занятиям по списку занятий.
     *
     * @param ids
     * @return
     */
    @Override
    @PostMapping("/getLessonsInfoByIds")
    public List<Lesson> getLessonsInfoByIds(@RequestBody final List<Long> ids) {
        return delegate.getLessonsInfoByIds(ids);
    }

    /**
     * Получение id учителей по id занятия.
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/getTeachersIdByLessonId/{id}")
    public List<Long> getTeachersIdByLessonId(@PathVariable("id") final long id) {
        return delegate.getTeachersIdByLessonId(id);
    }

    /**
     * Создание занятия.
     *
     * @param lessonForm
     * @return lesson
     */
    @Override
    @PostMapping("/createLesson")
    public Lesson createLesson(@RequestBody final LessonForm lessonForm) {
        return delegate.createLesson(lessonForm);
    }

    /**
     * Получение списка групп, которые связаны с уроком.
     *
     * @param id
     * @return list
     */
    @Override
    @GetMapping("/getGroupsIdsByLessonId/{id}")
    public List<Long> getGroupsIdsByLessonId(@PathVariable("id") final long id) {
        return delegate.getGroupsIdsByLessonId(id);
    }

}
