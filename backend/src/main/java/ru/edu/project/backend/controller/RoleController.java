package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.service.RoleServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController implements RoleService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private RoleServiceLayer delegate;

    /**
     * Получение информации о роли по ее коду.
     *
     * @param code
     * @return Role
     */
    @Override
    @GetMapping("/getRole/{code}")
    public Role getRole(@PathVariable("code") final EnumRoles code) {
        return delegate.getRole(code);
    }

    /**
     * Получение всех ролей.
     *
     * @return список
     */
    @Override
    @GetMapping("/getRoles")
    public List<Role> getRoles() {
        return delegate.getRoles();
    }

    /**
     * Получение роли пользователя.
     *
     * @param id
     * @return Role
     */
    @Override
    @GetMapping("/getRoleByUserId/{id}")
    public Role getRoleByUserId(@PathVariable("id") final long id) {
        return delegate.getRoleByUserId(id);
    }

    /**
     * Получение пользователей по роли.
     *
     * @param code
     */
    @Override
    @GetMapping("/getUsersByRole/{code}")
    public List<Long> getUsersByRole(@PathVariable("code") final EnumRoles code) {
        System.out.println(code.getCode());
        return delegate.getUsersByRole(code);
    }
}
