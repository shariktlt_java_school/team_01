package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.service.TaskServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController implements TaskService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private TaskServiceLayer delegate;

    /**
     * Получение информации по заданию.
     *
     * @param id
     * @return задание
     */
    @Override
    @GetMapping("/getTask/{id}")
    public Task getTask(@PathVariable("id") final long id) {
        return delegate.getTask(id);
    }

    /**
     * Получение списка занятий к заданию.
     *
     * @param id
     * @return list
     */
    @Override
    @GetMapping("/getLessonsIdByTaskId/{id}")
    public List<Long> getLessonsIdByTaskId(@PathVariable("id") final long id) {
        return delegate.getLessonsIdByTaskId(id);
    }

    /**
     * Получение заданий по списку заданий.
     *
     * @param ids
     * @return список
     */
    @Override
    @PostMapping("/getTasks")
    public List<Task> getTasks(@RequestBody final List<Long> ids) {
        return delegate.getTasks(ids);
    }

    /**
     * Получение всех заданий.
     *
     * @return список
     */
    @Override
    @GetMapping("/getAllTasks")
    public List<Task> getAllTasks() {
        return delegate.getAllTasks();
    }

    /**
     * Получение всех заданий по занятию.
     *
     * @param id
     * @return список
     */
    @Override
    @GetMapping("/getTasksByLessonId/{id}")
    public List<Task> getTasksByLessonId(@PathVariable("id") final long id) {
        return delegate.getTasksByLessonId(id);
    }

    /**
     * Получение всех заданий по занятиям.
     *
     * @param ids
     * @return список
     */
    @Override
    @PostMapping("/getTasksByLessonsId")
    public List<Task> getTasksByLessonsId(@RequestBody final List<Long> ids) {
        return delegate.getTasksByLessonsId(ids);
    }

    /**
     * Получение id всех сданных заданий студентом.
     *
     * @param id
     * @return список
     */
    @Override
    @GetMapping("/getProgressTasksIdsByStudentId/{id}")
    public List<Long> getProgressTasksIdsByStudentId(@PathVariable("id") final long id) {
        return delegate.getProgressTasksIdsByStudentId(id);
    }

    /**
     * Получение решения по id студента и id задания.
     *
     * @param studentId
     * @param taskId
     * @return
     */
    @Override
    @GetMapping("/getProgress/{studentId}/{taskId}")
    public StudentProgress getProgress(
            @PathVariable("studentId") final long studentId,
            @PathVariable("taskId") final long taskId) {
        return delegate.getProgress(studentId, taskId);
    }

    /**
     * Получение всех решений студентом.
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/getProgressByStudentId/{id}")
    public List<StudentProgress> getProgressByStudentId(@PathVariable("id") final long id) {
        return delegate.getProgressByStudentId(id);
    }

    /**
     *
     * @param taskForm
     * @return lesson
     */
    @Override
    @PostMapping("/createTask")
    public Task createTask(@RequestBody final TaskForm taskForm) {
        return delegate.createTask(taskForm);
    }

    /**
     * Создание ответа студента.
     *
     * @param progress
     * @return progress
     */
    @Override
    @PostMapping("/createProgress")
    public StudentProgress createProgress(@RequestBody final StudentProgress progress) {
        return delegate.createProgress(progress);
    }

    /**
     * Редактирование ответа студента.
     *
     * @param progress
     * @return progress
     */
    @Override
    @PostMapping("/updateProgress")
    public StudentProgress updateProgress(@RequestBody final StudentProgress progress) {
        return delegate.updateProgress(progress);
    }
}
