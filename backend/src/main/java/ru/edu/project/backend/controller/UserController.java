package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserForm;
import ru.edu.project.backend.api.users.UserService;
import ru.edu.project.backend.service.UserServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController implements UserService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private UserServiceLayer delegate;

    /**
     *
     * @param user
     * @return long
     */
    @Override
    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Long register(@RequestBody final UserForm user) {
        return delegate.register(user);
    }

    /**
     * Получение данных о пользователе.
     *
     * @param login
     * @return username
     */
    @Override
    @GetMapping("/loadUserByLogin/{username}")
    public User loadUserByLogin(@PathVariable("username") final String login) {
        return delegate.loadUserByLogin(login);
    }

    /**
     * Возвращает данные о пользователе.
     *
     * @param id
     * @return пользователь
     */
    @Override
    @GetMapping("/getUser/{id}")
    public User getUser(@PathVariable("id") final long id) {
        return delegate.getUser(id);
    }

    /**
     * Возвращает список пользователей по списку id.
     *
     * @param ids
     * @return список
     */
    @Override
    @PostMapping("/getUsers")
    public List<User> getUsers(@RequestBody final List<Long> ids) {
        return delegate.getUsers(ids);
    }

    /**
     * Регистрация нового пользователя.
     *
     * @param userForm
     * @return запись
     */
    @Override
    @PostMapping(value = "/createUser", consumes = MediaType.APPLICATION_JSON_VALUE)
    public User createUser(@RequestBody final UserForm userForm) {
        return delegate.createUser(userForm);
    }
}
