package ru.edu.project.backend.da;

import ru.edu.project.backend.api.groups.Group;

import java.util.List;

public interface GroupDALayer {

    /**
     * Связывание студента и группы.
     *
     * @param userId
     * @param groupId
     */
    void linkGroup(long userId, long groupId);

    /**
     * Получение информации о группе.
     *
     * @param id
     * @return информация о группе
     */
    Group getGroup(long id);

    /**
     * Получение групп, к которым принадлежит студент.
     *
     * @param id
     * @return список
     */
    List<Group> getGroupsByStudentId(long id);

    /**
     * Получение id студентов, которые принадлежат к группе.
     *
     * @param id
     * @return список
     */
    List<Long> getStudentsByGroupId(long id);

    /**
     * Получение всех групп.
     *
     * @return список
     */
    List<Group> getAllGroups();

    /**
     * Сохранение группы.
     *
     * @param group
     * @return request
     */
    Group create(Group group);
}
