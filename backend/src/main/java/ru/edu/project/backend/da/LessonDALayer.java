package ru.edu.project.backend.da;

import ru.edu.project.backend.api.lesson.Lesson;

import java.util.List;

public interface LessonDALayer {

    /**
     * Получение информации о занятии.
     *
     * @param id
     * @return Lesson
     */
    Lesson getLesson(long id);

    /**
     * Получение всех занятий по группе.
     * @param id
     * @return информация о группе
     */
    List<Lesson> getLessonsByGroupId(long id);

    /**
     *  Получение занятий по id пользователя(учителя).
     *
     * @param id
     * @return список
     */
    List<Lesson> getLessonsByUserId(long id);

    /**
     * Получение информации по занятиям по списку занятий.
     * @param ids
     * @return список
     */
    List<Lesson> getLessonsInfoByIds(List<Long> ids);

    /**
     * Получение id учителей по id занятия.
     *
     * @param id
     * @return список
     */
    List<Long> getTeachersIdByLessonId(long id);

    /**
     * Регистрация нового занятия.
     *
     * @param draft
     * @return запись
     */
    Lesson createLesson(Lesson draft);

    /**
     * Связывание преподавателя и занятия.
     *
     * @param teacherId
     * @param lessonId
     */
    void linkTeacher(long teacherId, long lessonId);

    /**
     * Связывание занятия и групп.
     *
     * @param lessonId
     * @param groupIds
     */
    void linkGroups(long lessonId, long groupIds);


    /**
     * Получение списка групп, которые связаны с уроком.
     *
     * @param id
     * @return list
     */
    List<Long> getGroupsIdsByLessonId(long id);
}
