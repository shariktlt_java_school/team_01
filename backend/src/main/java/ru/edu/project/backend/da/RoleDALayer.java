package ru.edu.project.backend.da;

import ru.edu.project.backend.api.roles.Role;

import java.util.List;

public interface RoleDALayer {

    /**
     * Связывание пользователя и роли.
     *
     * @param userId
     * @param roleId
     */
    void linkRole(long userId, long roleId);

    /**
     * Получение информации о роли.
     *
     * @param id
     * @return роль
     */
    Role getRole(long id);

    /**
     * Получение всех ролей.
     *
     * @return список
     */
    List<Role> getRoles();

    /**
     * Получение роли пользователя.
     *
     * @param id
     * @return Role
     */
    Role getRoleByUserId(long id);

    /**
     * Получение пользователей по роли.
     *
     * @param id
     * @return список
     */
    List<Long> getUsersByRole(long id);

}
