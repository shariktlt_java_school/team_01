package ru.edu.project.backend.da;

import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;

import java.util.List;

public interface TaskDALayer {

    /**
     * Получение информации по заданию.
     *
     * @param id
     * @return задание
     */
    Task getTask(long id);

    /**
     * Получение заданий по списку заданий.
     *
     * @param ids
     * @return список
     */
    List<Task> getTasks(List<Long> ids);

    /**
     * Получение всех заданий.
     *
     * @return список
     */
    List<Task> getAllTasks();

    /**
     * Получение всех заданий по занятию.
     *
     * @param id
     * @return список
     */
    List<Task> getTasksByLessonId(long id);

    /**
     * Получение всех заданий по занятиям.
     *
     * @param ids
     * @return список
     */
    List<Task> getTasksByLessonsId(List<Long> ids);

    /**
     * Получение id всех сданных заданий студентом.
     *
     * @param id
     * @return список
     */
    List<Long> getProgressTasksIdsByStudentId(long id);

    /**
     * Получение решения по id студента и id задания.
     *
     * @param studentId
     * @param taskId
     * @return прогресс студента
     */

    StudentProgress getProgress(long studentId, long taskId);

    /**
     * Получение всех решений студентом.
     * @param id
     * @return id
     */
    List<StudentProgress> getProgressByStudentId(long id);

    /**
     * Регистрация нового задания.
     *
     * @param draft
     * @return запись
     */
    Task createTask(Task draft);

    /**
     * Связывание задания и урока.
     *
     * @param idTask
     * @param idLesson
     */
    void linkLesson(long idTask, long idLesson);

    /**
     * Создание/обновление ответа студента.
     *
     * @param progress
     * @return progress
     */
    StudentProgress saveProgress(StudentProgress progress);

    /**
     * Получение списка занятий к заданию.
     * @param id
     * @return list
     */
    List<Long> getLessonsIdByTaskId(long id);
}
