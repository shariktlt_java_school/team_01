package ru.edu.project.backend.da;

import ru.edu.project.backend.api.users.User;

import java.util.List;

public interface UserDALayer {

    /**
     * Возвращает данные о пользователе.
     *
     * @param id
     * @return пользователь
     */
    User getUser(long id);

    /**
     * Возвращает список пользователей по списку id.
     *
     * @param ids
     * @return список
     */
    List<User> getUsers(List<Long> ids);

    /**
     * Регистрация нового пользователя.
     *
     * @param draft
     * @return запись
     */
    User createUser(User draft);

    /**
     * Поиск пользователя.
     *
     * @param login
     * @return userInfo
     */
    User loadUserByLogin(String login);

}
