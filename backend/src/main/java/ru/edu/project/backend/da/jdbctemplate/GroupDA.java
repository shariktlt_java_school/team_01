package ru.edu.project.backend.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.GroupDALayer;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data Access слой для group_table.
 */
@Component
@Profile("JDBC_TEMPLATE")
public class GroupDA implements GroupDALayer {

    /**
     * Запрос для связки студента и группы.
     */
    public static final String QUERY_FOR_LINK = "INSERT INTO student_group (user_id, group_id) VALUES(?, ?)";

    /**
     * Запрос для поиска группы по id.
     */
    public static final String QUERY_AVAILABLE_BY_ID = "SELECT * FROM \"groups\" WHERE id = ?";

    /**
     * Запрос для поиска всех групп.
     */
    public static final String QUERY_ALL_GROUP = "SELECT * FROM \"groups\"";

    /**
     * Запрос для поиска групп, к которым принадлежит студент.
     */
    public static final String QUERY_AVAILABLE_GROUP_BY_STUDENT_ID =
            "SELECT g.* FROM \"groups\" g "
                    + "LEFT JOIN student_group st "
                    + "ON g.id = st.group_id WHERE st.user_id = ?";

    /**
     * Запрос для поиска списка студентов, которые принадлежат к группе.
     */
    public static final String QUERY_AVAILABLE_STUDENT_ID_BY_GROUP_ID =
            "SELECT * FROM student_group where group_id = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("groups")
                .usingGeneratedKeyColumns("id");
    }

    /**
     * Связывание студента и группы.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void linkGroup(final long userId, final long groupId) {
        jdbcTemplate.update(QUERY_FOR_LINK, userId, groupId);
    }

    /**
     * Получение информации о группе.
     *
     * @param id
     * @return информация о группе
     */
    @Override
    public Group getGroup(final long id) {
        return jdbcTemplate.query(QUERY_AVAILABLE_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Получение групп, к которым принадлежит студент.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Group> getGroupsByStudentId(final long id) {
        return jdbcTemplate.query(QUERY_AVAILABLE_GROUP_BY_STUDENT_ID, this::rowMapper, id);
    }

    /**
     * Получение id студентов, которые принадлежат к группе.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Long> getStudentsByGroupId(final long id) {
        return jdbcTemplate.query(QUERY_AVAILABLE_STUDENT_ID_BY_GROUP_ID, this::rowMapperLong, id);
    }

    /**
     * Получение всех групп.
     *
     * @return список
     */
    @Override
    public List<Group> getAllGroups() {
        return jdbcTemplate.query(QUERY_ALL_GROUP, this::rowMapper);
    }

    /**
     * Сохранение (создание/обновление) группы.
     *
     * @param group
     * @return group
     */
    @Override
    public Group create(final Group group) {
        long id = jdbcInsert.executeAndReturnKey(toMap(group)).longValue();
        group.setId(id);
        return group;
    }

    private Map<String, Object> toMap(final Group draft) {
        HashMap<String, Object> map = new HashMap<>();
        if (draft.getId() != null) {
            map.put("id", draft.getId());
        }
        map.put("title", draft.getTitle());

        return map;
    }

    @SneakyThrows
    private Long rowMapperLong(final ResultSet resultSet, final int pos) {
        //resultSet.next();
        return mapRowLong(resultSet);
    }

    @SneakyThrows
    private Long mapRowLong(final ResultSet resultSet) {
        return resultSet.getLong("user_id");
    }

    @SneakyThrows
    private Group rowMapper(final ResultSet resultSet, final int pos) {
        return mapRow(resultSet);
    }

    @SneakyThrows
    private Group singleRowMapper(final ResultSet resultSet) {
        resultSet.next();
        return mapRow(resultSet);
    }

    @SneakyThrows
    private Group mapRow(final ResultSet resultSet) {
        return Group.builder()
                .id(resultSet.getLong("id"))
                .title(resultSet.getString("title"))
                .build();
    }
}
