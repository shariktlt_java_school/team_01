package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.GroupDALayer;
import ru.edu.project.backend.da.jpa.converter.group.GroupMapper;
import ru.edu.project.backend.da.jpa.converter.group.StudentGroupMapper;
import ru.edu.project.backend.da.jpa.entity.group.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.group.StudentGroupEntity;
import ru.edu.project.backend.da.jpa.repository.group.GroupEntityRepository;
import ru.edu.project.backend.da.jpa.repository.group.StudentGroupEntityRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile("SPRING_DATA")
@Service
public class JPAGroupDA implements GroupDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private GroupEntityRepository groupRepo;

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private StudentGroupEntityRepository studentGroupRepo;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private StudentGroupMapper studentGroupMapper;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private GroupMapper mapper;

    /**
     * Связывание студента и группы.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void linkGroup(final long userId, final long groupId) {
        StudentGroupEntity entityDraft = new StudentGroupEntity();
        entityDraft.setPk(StudentGroupEntity.pk(userId, groupId));

        studentGroupRepo.save(entityDraft);
    }

    /**
     * Получение информации о группе.
     *
     * @param id
     * @return информация о группе
     */
    @Override
    public Group getGroup(final long id) {
        Optional<GroupEntity> entity = groupRepo.findById(id);
        return entity.map(groupEntity -> mapper.map(groupEntity)).orElse(null);
    }

    /**
     * Получение групп, к которым принадлежит студент.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Group> getGroupsByStudentId(final long id) {
        List<StudentGroupEntity> entities = studentGroupRepo.findAllByPkUserId(id);
        return mapper.map(entities.stream().map(StudentGroupEntity::getGroup).collect(Collectors.toList()));
    }

    /**
     * Получение id студентов, которые принадлежат к группе.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Long> getStudentsByGroupId(final long id) {
        List<StudentGroupEntity> groupStudentEntities = studentGroupRepo.findAllByPkGroupId(id);
        List<Long> result = studentGroupMapper.map(groupStudentEntities).stream().map(studentGroup -> studentGroup.getUserId()).collect(Collectors.toList());
        return result;
    }

    /**
     * Получение всех групп.
     *
     * @return список
     */
    @Override
    public List<Group> getAllGroups() {
        Iterable<GroupEntity> allGroups = groupRepo.findAll();
        return mapper.map(allGroups);
    }

    /**
     * Сохранение группы.
     *
     * @param group
     * @return request
     */
    @Override
    public Group create(final Group group) {
        GroupEntity entity = mapper.map(group);

        GroupEntity saved = groupRepo.save(entity);
        group.setId(saved.getId());
        return mapper.map(saved);
    }
}
