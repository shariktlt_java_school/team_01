package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.da.LessonDALayer;
import ru.edu.project.backend.da.jpa.converter.lesson.LessonGroupMapper;
import ru.edu.project.backend.da.jpa.converter.lesson.LessonMapper;
import ru.edu.project.backend.da.jpa.converter.lesson.LessonTeacherMapper;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonEntity;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonGroupEntity;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonTeacherEntity;
import ru.edu.project.backend.da.jpa.repository.lesson.LessonEntityRepository;
import ru.edu.project.backend.da.jpa.repository.lesson.LessonGroupEntityRepository;
import ru.edu.project.backend.da.jpa.repository.lesson.LessonTeacherEntityRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile("SPRING_DATA")
@Service
public class JPALessonDA implements LessonDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private LessonEntityRepository repository;

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private LessonGroupEntityRepository lessonGroupRepository;

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private LessonTeacherEntityRepository lessonTeacherRepository;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private LessonGroupMapper lessonGroupMapper;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private LessonTeacherMapper lessonTeacherMapper;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private LessonMapper mapper;
    /**
     * Получение информации о занятии.
     *
     * @param id
     * @return Lesson
     */
    @Override
    public Lesson getLesson(final long id) {
        Optional<LessonEntity> entity = repository.findById(id);
        return entity.map(lessonEntity -> mapper.map(lessonEntity)).orElse(null);
    }

    /**
     * Получение всех занятий по группе.
     *
     * @param id
     * @return информация о группе
     */
    @Override
    public List<Lesson> getLessonsByGroupId(final long id) {
        List<LessonGroupEntity> entities = lessonGroupRepository.findAllByPkIdGroup(id);
        return mapper.map(entities.stream().map(LessonGroupEntity::getLesson).collect(Collectors.toList()));
    }

    /**
     * Получение занятий по id пользователя(учителя).
     *
     * @param id
     * @return список
     */
    @Override
    public List<Lesson> getLessonsByUserId(final long id) {
        List<LessonTeacherEntity> entities = lessonTeacherRepository.findAllByPkIdTeacher(id);
        return mapper.map(entities.stream().map(LessonTeacherEntity::getLesson).collect(Collectors.toList()));
    }

    /**
     * Получение информации по занятиям по списку занятий.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Lesson> getLessonsInfoByIds(final List<Long> ids) {
        return mapper.map(repository.findAllById(ids));
    }

    /**
     * Получение id учителей по id занятия.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Long> getTeachersIdByLessonId(final long id) {
        List<LessonTeacherEntity> entities = lessonTeacherRepository.findAllByPkIdLesson(id);
        List<Long> result = lessonTeacherMapper.map(entities).stream().map(lessonTeacher -> lessonTeacher.getIdTeacher()).collect(Collectors.toList());
        return result;
    }

    /**
     * Создание занятия.
     *
     * @param draft
     * @return lesson
     */
    @Override
    public Lesson createLesson(final Lesson draft) {
        LessonEntity entity = mapper.map(draft);

        LessonEntity saved = repository.save(entity);
        draft.setId(saved.getId());

        return mapper.map(saved);
    }

    /**
     * Связывание учителя и занятия.
     *
     * @param teacherId
     * @param lessonId
     */
    @Override
    public void linkTeacher(final long teacherId, final long lessonId) {
        LessonTeacherEntity entityDraft = new LessonTeacherEntity();
        entityDraft.setPk(LessonTeacherEntity.pk(lessonId, teacherId));

        lessonTeacherRepository.save(entityDraft);
    }

    /**
     * Связывание занятия и группы.
     * @param lessonId
     * @param groupId
     */
    @Override
    public void linkGroups(final long lessonId, final long groupId) {
        LessonGroupEntity entityDraft = new LessonGroupEntity();
        entityDraft.setPk(LessonGroupEntity.pk(lessonId, groupId));

        lessonGroupRepository.save(entityDraft);
    }

    /**
     * Получение списка групп, которые связаны с уроком.
     *
     * @param id
     * @return list
     */
    @Override
    public List<Long> getGroupsIdsByLessonId(final long id) {
        List<LessonGroupEntity> entities = lessonGroupRepository.findAllByPkIdLesson(id);
        return lessonGroupMapper.map(entities).stream().map(lessonGroup -> lessonGroup.getIdGroup()).collect(Collectors.toList());
    }
}
