package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.da.RoleDALayer;
import ru.edu.project.backend.da.jpa.converter.role.RoleMapper;
import ru.edu.project.backend.da.jpa.converter.role.RoleUserMapper;
import ru.edu.project.backend.da.jpa.entity.role.RoleEntity;
import ru.edu.project.backend.da.jpa.entity.role.RoleUserEntity;
import ru.edu.project.backend.da.jpa.repository.role.RoleEntityRepository;
import ru.edu.project.backend.da.jpa.repository.role.RoleUserEntityRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile("SPRING_DATA")
@Service
public class JPARoleDA implements RoleDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private RoleEntityRepository repository;

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private RoleUserEntityRepository roleUserRepository;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private RoleMapper mapper;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private RoleUserMapper mapperRoleUser;

    /**
     * Связывание пользователя и роли.
     *
     * @param userId
     * @param roleId
     */
    @Override
    public void linkRole(final long userId, final long roleId) {
        RoleUserEntity entityDraft = new RoleUserEntity();
        entityDraft.setPk(RoleUserEntity.pk(userId, roleId));

        roleUserRepository.save(entityDraft);
    }

    /**
     * Получение информации о роли.
     *
     * @param id
     * @return роль
     */
    @Override
    public Role getRole(final long id) {
        Optional<RoleEntity> entity = repository.findById(id);
        return entity.map(groupEntity -> mapper.map(groupEntity)).orElse(null);
    }

    /**
     * Получение всех ролей.
     *
     * @return список
     */
    @Override
    public List<Role> getRoles() {
        Iterable<RoleEntity> allGroups = repository.findAll();
        return mapper.map(allGroups);
    }

    /**
     * Получение роли пользователя.
     *
     * @param id
     * @return Role
     */
    @Override
    public Role getRoleByUserId(final long id) {
        /*RoleUserEntity entity = roleUserRepository.findFirstByPkIdRole(id);
        return mapper.map(entity.getRole());*/
        List<RoleUserEntity> entity = roleUserRepository.findAllByPkIdUser(id);
        List<RoleEntity> collect = entity.stream().map(RoleUserEntity::getRole).collect(Collectors.toList());
        List<Role> map = mapper.map(collect);
        return map.get(0);
    }

    /**
     * Получение пользователей по роли.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Long> getUsersByRole(final long id) {
        List<RoleUserEntity> entity = roleUserRepository.findAllByPkIdRole(id);
        return mapperRoleUser.map(entity).stream().map(roleUser -> roleUser.getIdUser()).collect(Collectors.toList());
    }
}
