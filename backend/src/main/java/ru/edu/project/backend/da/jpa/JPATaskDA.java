package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.da.TaskDALayer;
import ru.edu.project.backend.da.jpa.converter.task.StudentProgressMapper;
import ru.edu.project.backend.da.jpa.converter.task.TaskLessonMapper;
import ru.edu.project.backend.da.jpa.converter.task.TaskMapper;
import ru.edu.project.backend.da.jpa.entity.task.StudentProgressEntity;
import ru.edu.project.backend.da.jpa.entity.task.TaskEntity;
import ru.edu.project.backend.da.jpa.entity.task.TaskLessonEntity;
import ru.edu.project.backend.da.jpa.repository.task.StudentProgressEntityRepository;
import ru.edu.project.backend.da.jpa.repository.task.TaskEntityRepository;
import ru.edu.project.backend.da.jpa.repository.task.TaskLessonEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile("SPRING_DATA")
@Service
public class JPATaskDA implements TaskDALayer {
    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private TaskEntityRepository repository;

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private TaskLessonEntityRepository taskLessonRepository;

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private StudentProgressEntityRepository studentRepository;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private TaskLessonMapper taskLessonMapper;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private StudentProgressMapper progressMapper;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private TaskMapper mapper;

    /**
     * Получение информации по заданию.
     *
     * @param id
     * @return задание
     */
    @Override
    public Task getTask(final long id) {
        Optional<TaskEntity> entity = repository.findById(id);
        return entity.map(lessonEntity -> mapper.map(lessonEntity)).orElse(null);
    }

    /**
     * Получение заданий по списку заданий.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Task> getTasks(final List<Long> ids) {
        return mapper.map(repository.findAllById(ids));
    }

    /**
     * Получение всех заданий.
     *
     * @return список
     */
    @Override
    public List<Task> getAllTasks() {
        Iterable<TaskEntity> allGroups = repository.findAll();
        return mapper.map(allGroups);
    }

    /**
     * Получение всех заданий по занятию.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Task> getTasksByLessonId(final long id) {
        List<TaskLessonEntity> entities = taskLessonRepository.findAllByPkIdLesson(id);
        return mapper.map(entities.stream().map(TaskLessonEntity::getTask).collect(Collectors.toList()));
    }

    /**
     * Получение всех заданий по занятиям.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Task> getTasksByLessonsId(final List<Long> ids) {
        List<TaskLessonEntity> entities = new ArrayList<>();
        ids.forEach(id -> taskLessonRepository.findAllByPkIdLesson(id).forEach(taskLessonEntity -> entities.add(taskLessonEntity)));

        return mapper.map(entities.stream().map(TaskLessonEntity::getTask).collect(Collectors.toList()));
    }

    /**
     * Получение id всех сданных заданий студентом.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Long> getProgressTasksIdsByStudentId(final long id) {
        return null;
    }

    /**
     * Получение решения по id студента и id задания.
     *
     * @param studentId
     * @param taskId
     * @return прогресс студента
     */
    @Override
    public StudentProgress getProgress(final long studentId, final long taskId) {
        Optional<StudentProgressEntity> res = studentRepository.findById(StudentProgressEntity.pk(studentId, taskId));
        return res.map(progress -> progressMapper.map(progress)).orElse(null);
    }

    /**
     * Получение всех решений студентом.
     *
     * @param id
     * @return id
     */
    @Override
    public List<StudentProgress> getProgressByStudentId(final long id) {
        return progressMapper.map(studentRepository.findAllByPkIdStudent(id));
    }

    /**
     *
     * @param draft
     * @return task
     */
    @Override
    public Task createTask(final Task draft) {
        TaskEntity entity = mapper.map(draft);

        TaskEntity saved = repository.save(entity);
        draft.setId(saved.getId());

        return mapper.map(saved);
    }
    /**
     *
     * @param idTask
     * @param idLesson
     */
    @Override
    public void linkLesson(final long idTask, final long idLesson) {
        TaskLessonEntity entityDraft = new TaskLessonEntity();
        entityDraft.setPk(TaskLessonEntity.pk(idTask, idLesson));
        taskLessonRepository.save(entityDraft);
    }


    /**
     * Редактирование ответа студента.
     *
     * @param progress
     * @return progress
     */
    @Override
    public StudentProgress saveProgress(final StudentProgress progress) {
        StudentProgressEntity entity = progressMapper.map(progress);
        StudentProgressEntity saved = studentRepository.save(entity);

        return progressMapper.map(saved);
    }

    /**
     * Получение списка занятий к заданию.
     *
     * @param id
     * @return list
     */
    @Override
    public List<Long> getLessonsIdByTaskId(final long id) {
        List<TaskLessonEntity> entities = taskLessonRepository.findAllByPkIdTask(id);

        return taskLessonMapper.map(entities).stream().map(taskLesson -> taskLesson.getIdLesson()).collect(Collectors.toList());
    }
}
