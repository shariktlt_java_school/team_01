package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.da.UserDALayer;
import ru.edu.project.backend.da.jpa.converter.user.UserMapper;
import ru.edu.project.backend.da.jpa.entity.user.UserEntity;
import ru.edu.project.backend.da.jpa.repository.user.UserEntityRepository;

import java.util.List;
import java.util.Optional;

@Profile("SPRING_DATA")
@Service
public class JPAUserDA implements UserDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private UserEntityRepository repository;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private UserMapper mapper;

    /**
     * Возвращает данные о пользователе.
     *
     * @param id
     * @return пользователь
     */
    @Override
    public User getUser(final long id) {
        Optional<UserEntity> entity = repository.findById(id);
        return entity.map(groupEntity -> mapper.map(groupEntity)).orElse(null);
    }

    /**
     * Возвращает список пользователей по списку id.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<User> getUsers(final List<Long> ids) {
        return mapper.map(repository.findAllById(ids));
    }

    /**
     * Регистрация нового пользователя.
     *
     * @param draft
     * @return запись
     */
    @Override
    public User createUser(final User draft) {
        UserEntity entity = mapper.map(draft);

        UserEntity saved = repository.save(entity);
        draft.setId(saved.getId());
        return draft;
    }

    /**
     * Поиск пользователя.
     *
     * @param login
     * @return user
     */
    @Override
    public User loadUserByLogin(final String login) {
        return mapper.map(repository.findByLogin(login));
    }
}
