package ru.edu.project.backend.da.jpa.converter.group;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.jpa.entity.group.GroupEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupMapper {

    /**
     * Маппер GroupEntity -> Group.
     *
     * @param entity
     * @return group
     */
    Group map(GroupEntity entity);

    /**
     * Маппер Group -> GroupEntity.
     *
     * @param entity
     * @return group
     */
    GroupEntity map(Group entity);

    /**
     * Маппер List<GroupEntity> -> List<Group>.
     * @param ids
     * @return list Group
     */
    List<Group> map(Iterable<GroupEntity> ids);
}
