package ru.edu.project.backend.da.jpa.converter.group;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.edu.project.backend.da.jpa.entity.group.StudentGroupEntity;
import ru.edu.project.backend.model.StudentGroup;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentGroupMapper {

    /**
     * Маппер StudentGroupEntity -> StudentGroup.
     *
     * @param entity
     * @return group
     */
    @Mapping(source = "pk.userId", target = "userId")
    @Mapping(source = "pk.groupId", target = "groupId")
    StudentGroup map(StudentGroupEntity entity);

    /**
     * Маппер List<StudentGroupEntity> -> List<StudentGroup>.
     * @param ids
     * @return list Group
     */
    @Mapping(source = "pk.userId", target = "userId")
    @Mapping(source = "pk.groupId", target = "groupId")
    List<StudentGroup> map(Iterable<StudentGroupEntity> ids);
}
