package ru.edu.project.backend.da.jpa.converter.lesson;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonTeacherEntity;
import ru.edu.project.backend.model.LessonTeacher;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonTeacherMapper {
    /**
     * Маппер LessonTeacherEntity -> LessonTeacher.
     *
     * @param entity
     * @return group
     */
    @Mapping(source = "pk.idLesson", target = "idLesson")
    @Mapping(source = "pk.idTeacher", target = "idTeacher")
    LessonTeacher map(LessonTeacherEntity entity);

    /**
     * Маппер List<LessonTeacherEntity> -> List<LessonTeacher>.
     * @param ids
     * @return list Group
     */
    @Mapping(source = "pk.idLesson", target = "idLesson")
    @Mapping(source = "pk.idTeacher", target = "idTeacher")
    List<LessonTeacher> map(Iterable<LessonTeacherEntity> ids);
}
