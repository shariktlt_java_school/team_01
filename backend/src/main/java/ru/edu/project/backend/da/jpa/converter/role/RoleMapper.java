package ru.edu.project.backend.da.jpa.converter.role;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.da.jpa.entity.role.RoleEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    /**
     * Маппер UserEntity -> User.
     *
     * @param entity
     * @return group
     */
    @Mapping(source = "title", target = "name")
    Role map(RoleEntity entity);

    /**
     * Маппер List<RoleEntity> -> List<Role>.
     * @param ids
     * @return list Role
     */
    List<Role> map(Iterable<RoleEntity> ids);

}
