package ru.edu.project.backend.da.jpa.converter.role;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.edu.project.backend.da.jpa.entity.role.RoleUserEntity;
import ru.edu.project.backend.model.RoleUser;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleUserMapper {

    /**
     * Маппер RoleUserEntity -> RoleUser.
     *
     * @param entity
     * @return group
     */
    @Mapping(source = "pk.idUser", target = "idUser")
    @Mapping(source = "pk.idRole", target = "idRole")
    RoleUser map(RoleUserEntity entity);

    /**
     * Маппер List<StudentGroupEntity> -> List<StudentGroup>.
     * @param ids
     * @return list Group
     */
    @Mapping(source = "pk.idUser", target = "idUser")
    @Mapping(source = "pk.idRole", target = "idRole")
    List<RoleUser> map(Iterable<RoleUserEntity> ids);
}
