package ru.edu.project.backend.da.jpa.converter.task;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.da.jpa.entity.task.StudentProgressEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentProgressMapper {

    /**
     * Маппер StudentProgressEntity -> StudentProgress.
     *
     * @param entity
     * @return progress
     */
    @Mapping(source = "pk.idTask", target = "taskId")
    @Mapping(source = "pk.idStudent", target = "userId")
    StudentProgress map(StudentProgressEntity entity);

    /**
     * Маппер StudentProgress -> StudentProgressEntity.
     *
     * @param entity
     * @return progress
     */
    @Mapping(source = "taskId", target = "pk.idTask")
    @Mapping(source = "userId", target = "pk.idStudent")
    StudentProgressEntity map(StudentProgress entity);

    /**
     * Маппер List<StudentProgressEntity> -> List<StudentProgress>.
     * @param ids
     * @return list
     */
    @Mapping(source = "pk.idStudent", target = "userId")
    @Mapping(source = "pk.idTask", target = "taskId")
    List<StudentProgress> map(Iterable<StudentProgressEntity> ids);
}
