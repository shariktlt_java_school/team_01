package ru.edu.project.backend.da.jpa.converter.task;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.edu.project.backend.da.jpa.entity.task.TaskLessonEntity;
import ru.edu.project.backend.model.TaskLesson;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskLessonMapper {
    /**
     * Маппер LessonTeacherEntity -> LessonTeacher.
     *
     * @param entity
     * @return group
     */
    @Mapping(source = "pk.idTask", target = "idTask")
    @Mapping(source = "pk.idLesson", target = "idLesson")
    TaskLesson map(TaskLessonEntity entity);

    /**
     * Маппер List<LessonTeacherEntity> -> List<LessonTeacher>.
     * @param ids
     * @return list Group
     */
    @Mapping(source = "pk.idTask", target = "idTask")
    @Mapping(source = "pk.idLesson", target = "idLesson")
    List<TaskLesson> map(Iterable<TaskLessonEntity> ids);
}
