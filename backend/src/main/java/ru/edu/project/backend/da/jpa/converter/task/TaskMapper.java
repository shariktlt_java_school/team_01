package ru.edu.project.backend.da.jpa.converter.task;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.da.jpa.entity.task.TaskEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    /**
     * Маппер TaskEntity -> Task.
     *
     * @param entity
     * @return group
     */
    Task map(TaskEntity entity);

    /**
     * Маппер Task -> TaskEntity.
     *
     * @param entity
     * @return group
     */
    TaskEntity map(Task entity);

    /**
     * Маппер List<LessonEntity> -> List<Lesson>.
     * @param ids
     * @return list Role
     */
    List<Task> map(Iterable<TaskEntity> ids);
}
