package ru.edu.project.backend.da.jpa.converter.user;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.da.jpa.entity.user.UserEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    /**
     * Маппер UserEntity -> User.
     *
     * @param entity
     * @return group
     */
    User map(UserEntity entity);

    /**
     * Маппер User -> UserEntity.
     *
     * @param entity
     * @return group
     */
    UserEntity map(User entity);

    /**
     * Маппер List<UserEntity> -> List<User>.
     *
     * @param ids
     * @return list Group
     */
    List<User> map(Iterable<UserEntity> ids);
}

