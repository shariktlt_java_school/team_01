package ru.edu.project.backend.da.jpa.entity.group;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "\"GROUPS\"")
public class GroupEntity {

    /**
     * id группы.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "group_seq")
    @SequenceGenerator(name = "group_seq", sequenceName = "groups_id_sequence", allocationSize = 1)
    private Long id;

    /**
     * Наименования группы.
     */
    private String title;
}
