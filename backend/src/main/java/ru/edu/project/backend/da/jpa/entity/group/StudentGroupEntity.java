package ru.edu.project.backend.da.jpa.entity.group;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "student_group")
public class StudentGroupEntity {

    /**
     * Так как у нас составной ключ, необходимо вынести его в отдельную встраиваемую сущность.
     */
    @EmbeddedId
    private StudentGroupId pk;

    /**
     * Связь один к одному с таблицей GROUP через поле group_id.
     */
    @OneToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id", insertable = false, updatable = false)
    private GroupEntity group;

    /**
     * Встраиваемая сущность описывающая поля входящие в состав составного ключа таблицы.
     */
    @Embeddable
    @Getter
    @Setter
    public static class StudentGroupId implements Serializable {

        /**
         * Составная часть ключа user_id.
         */
        @Column(name = "user_id")
        private Long userId;

        /**
         * Составная часть ключа group_id.
         */
        @Column(name = "group_id")
        private Long groupId;
    }

    /**
     * Билдер первичного ключа.
     *
     * @param userId
     * @param groupId
     * @return pk
     */
    public static StudentGroupId pk(final long userId, final long groupId) {
        StudentGroupId id = new StudentGroupId();
        id.setUserId(userId);
        id.setGroupId(groupId);
        return id;
    }
}
