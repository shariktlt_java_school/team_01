package ru.edu.project.backend.da.jpa.entity.lesson;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "lessons")
public class LessonEntity {

    /**
     * id занятия.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "lesson_seq")
    @SequenceGenerator(name = "lesson_seq", sequenceName = "lessons_id_sequence", allocationSize = 1)
    private Long id;

    /**
     * Номер классной комнаты.
     */
    @Column(name = "classroom")
    private int classRoom;

    /**
     * Время начала занятий.
     */
    @Column(name = "start_time")
    private Timestamp startTime;

    /**
     * Время конца занятий.
     */
    @Column(name = "end_time")
    private Timestamp endTime;
}
