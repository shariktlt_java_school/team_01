package ru.edu.project.backend.da.jpa.entity.role;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "ROLE")
public class RoleEntity {
    /**
     * id пользователя.
     */
    @Id
    private Long id;

    /**
     * Наименование роли.
     */
    private String title;

}
