package ru.edu.project.backend.da.jpa.entity.role;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "role_user")
public class RoleUserEntity {

    /**
     * Так как у нас составной ключ, необходимо вынести его в отдельную встраиваемую сущность.
     */
    @EmbeddedId
    private RoleUserId pk;

    /**
     * Связь один к одному с таблицей ROLE через поле group_id.
     */
    @OneToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id", insertable = false, updatable = false)
    private RoleEntity role;

    /**
     * Встраиваемая сущность описывающая поля входящие в состав составного ключа таблицы.
     */
    @Embeddable
    @Getter
    @Setter
    public static class RoleUserId implements Serializable {

        /**
         * Составная часть ключа id_user.
         */
        @Column(name = "id_user")
        private Long idUser;

        /**
         * Составная часть ключа id_user.
         */
        @Column(name = "id_role")
        private Long idRole;
    }

    /**
     * Билдер первичного ключа.
     *
     * @param idUser
     * @param idRole
     * @return pk
     */
    public static RoleUserId pk(final long idUser, final long idRole) {
        RoleUserId id = new RoleUserId();
        id.setIdUser(idUser);
        id.setIdRole(idRole);
        return id;
    }

}

