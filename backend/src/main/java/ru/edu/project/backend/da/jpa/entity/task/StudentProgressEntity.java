package ru.edu.project.backend.da.jpa.entity.task;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "student_progress")
public class StudentProgressEntity {

    /**
     * Так как у нас составной ключ, необходимо вынести его в отдельную встраиваемую сущность.
     */
    @EmbeddedId
    private StudentProgressId pk;

    /**
     * Связь один к одному с таблицей TASKS через поле id_task.
     */
    @OneToOne
    @JoinColumn(name = "id_task", referencedColumnName = "id", insertable = false, updatable = false)
    private TaskEntity task;

    /**
     * Ответ студента на задание.
     */
    private String response;

    /**
     * Оценка задания.
     */
    private int assessment;

    /**
     * Было ли проверено задание.
     */
    private int verified;

    /**
     * Встраиваемая сущность описывающая поля входящие в состав составного ключа таблицы.
     */
    @Embeddable
    @Getter
    @Setter
    public static class StudentProgressId implements Serializable {

        /**
         * Составная часть ключа id_student.
         */
        @Column(name = "id_student")
        private Long idStudent;

        /**
         * Составная часть ключа id_task.
         */
        @Column(name = "id_task")
        private Long idTask;

    }

    /**
     * Билдер первичного ключа.
     *
     * @param idStudent
     * @param idTask
     * @return pk
     */
    public static StudentProgressId pk(final long idStudent, final long idTask) {
        StudentProgressId id = new StudentProgressId();
        id.setIdStudent(idStudent);
        id.setIdTask(idTask);

        return id;
    }
}
