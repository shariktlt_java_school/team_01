package ru.edu.project.backend.da.jpa.entity.task;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "TASKS")
public class TaskEntity {

    /**
     * id занятия.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "task_seq")
    @SequenceGenerator(name = "task_seq", sequenceName = "tasks_id_sequence", allocationSize = 1)
    private Long id;

    /**
     * Наименование задания.
     */
    private String name;

    /**
     * Текст задания.
     */
    private String description;

}
