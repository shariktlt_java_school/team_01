package ru.edu.project.backend.da.jpa.entity.task;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "task_lesson")
public class TaskLessonEntity {

    /**
     * Так как у нас составной ключ, необходимо вынести его в отдельную встраиваемую сущность.
     */
    @EmbeddedId
    private TaskLessonId pk;

    /**
     * Связь один к одному с таблицей LESSON через поле id_lesson.
     */
    @OneToOne
    @JoinColumn(name = "id_task", referencedColumnName = "id", insertable = false, updatable = false)
    private TaskEntity task;

    /**
     * Встраиваемая сущность описывающая поля входящие в состав составного ключа таблицы.
     */
    @Embeddable
    @Getter
    @Setter
    public static class TaskLessonId implements Serializable {

        /**
         * Составная часть ключа id_task.
         */
        @Column(name = "id_task")
        private Long idTask;

        /**
         * Составная часть ключа id_lesson.
         */
        @Column(name = "id_lesson")
        private Long idLesson;
    }

    /**
     * Билдер первичного ключа.
     *
     * @param idTask
     * @param idLesson
     * @return pk
     */
    public static TaskLessonId pk(final long idTask, final long idLesson) {
        TaskLessonId id = new TaskLessonId();
        id.setIdLesson(idLesson);
        id.setIdTask(idTask);

        return id;
    }

}
