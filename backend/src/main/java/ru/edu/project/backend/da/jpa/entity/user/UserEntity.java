package ru.edu.project.backend.da.jpa.entity.user;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "USERS")
public class UserEntity {

    /**
     * id пользователя.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "users_id_sequence", allocationSize = 1)
    private Long id;

    /**
     * Имя.
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * Фамилия.
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * Отчество.
     */
    private String patronymic;

    /**
     * День рождения.
     */
    @Column(name = "birth_day")
    private Timestamp birthday;

    /**
     * Логин.
     */
    private String login;

    /**
     * Пароль.
     */
    private String password;
}
