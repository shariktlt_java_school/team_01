package ru.edu.project.backend.da.jpa.repository.group;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.group.GroupEntity;

@Repository
public interface GroupEntityRepository extends CrudRepository<GroupEntity, Long> {

}
