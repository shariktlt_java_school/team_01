package ru.edu.project.backend.da.jpa.repository.group;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.group.StudentGroupEntity;

import java.util.List;

@Repository
public interface StudentGroupEntityRepository extends CrudRepository<StudentGroupEntity, StudentGroupEntity.StudentGroupId> {

    /**
     * Поиск записей по полю составного ключа pk.userId.
     *
     * @param userId
     * @return list entity
     */
    List<StudentGroupEntity> findAllByPkUserId(long userId);

    /**
     * Поиск записей по полю составного ключа pk.groupId.
     *
     * @param groupId
     * @return list entity
     */
    List<StudentGroupEntity> findAllByPkGroupId(long groupId);
}
