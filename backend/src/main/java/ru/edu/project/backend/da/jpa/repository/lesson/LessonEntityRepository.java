package ru.edu.project.backend.da.jpa.repository.lesson;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonEntity;

@Repository
public interface LessonEntityRepository extends CrudRepository<LessonEntity, Long> {

}
