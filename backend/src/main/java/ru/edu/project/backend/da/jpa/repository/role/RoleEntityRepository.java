package ru.edu.project.backend.da.jpa.repository.role;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.role.RoleEntity;

@Repository
public interface RoleEntityRepository extends CrudRepository<RoleEntity, Long> {

}
