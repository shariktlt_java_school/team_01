package ru.edu.project.backend.da.jpa.repository.role;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.role.RoleUserEntity;

import java.util.List;

@Repository
public interface RoleUserEntityRepository extends CrudRepository<RoleUserEntity, RoleUserEntity.RoleUserId> {

    /**
     * Поиск записей по полю составного ключа pk.idUser.
     *
     * @param idUser
     * @return list entity
     */
    List<RoleUserEntity> findAllByPkIdUser(long idUser);

    /**
     * Поиск первой записи по полю составного ключа pk.idUser.
     *
     * @param id
     * @return list entity
     */
    RoleUserEntity findFirstByPkIdRole(long id);

    /**
     * Поиск записей по полю составного ключа pk.roleId.
     *
     * @param idRole
     * @return list entity
     */
    List<RoleUserEntity> findAllByPkIdRole(long idRole);

}
