package ru.edu.project.backend.da.jpa.repository.task;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.task.StudentProgressEntity;

import java.util.List;

@Repository
public interface StudentProgressEntityRepository extends CrudRepository<StudentProgressEntity, StudentProgressEntity.StudentProgressId> {

    /**
     * Поиск записей по полю составного ключа pk.idTask.
     *
     * @param idTask
     * @return list entity
     */
    List<StudentProgressEntity> findAllByPkIdTask(long idTask);

    /**
     * Поиск записей по полю составного ключа pk.idStudent.
     *
     * @param idStudent
     * @return list entity
     */
    List<StudentProgressEntity> findAllByPkIdStudent(long idStudent);

}
