package ru.edu.project.backend.da.jpa.repository.task;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.task.TaskEntity;

@Repository
public interface TaskEntityRepository extends CrudRepository<TaskEntity, Long> {
}
