package ru.edu.project.backend.da.jpa.repository.task;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.task.TaskLessonEntity;

import java.util.List;

@Repository
public interface TaskLessonEntityRepository extends CrudRepository<TaskLessonEntity, TaskLessonEntity.TaskLessonId> {

    /**
     * Поиск записей по полю составного ключа pk.idTask.
     *
     * @param idTask
     * @return list entity
     */
    List<TaskLessonEntity> findAllByPkIdTask(long idTask);

    /**
     * Поиск записей по полю составного ключа pk.idLesson.
     *
     * @param idLesson
     * @return list entity
     */
    List<TaskLessonEntity> findAllByPkIdLesson(long idLesson);



}
