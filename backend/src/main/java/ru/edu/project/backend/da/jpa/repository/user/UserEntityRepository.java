package ru.edu.project.backend.da.jpa.repository.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.user.UserEntity;

@Repository
public interface UserEntityRepository  extends CrudRepository<UserEntity, Long> {
    /**
     * Поиск записей.
     *
     * @param login
     * @return userEntity
     */
    UserEntity findByLogin(String login);

}
