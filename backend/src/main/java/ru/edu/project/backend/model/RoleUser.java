package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class RoleUser {
    /**
     * Составная часть ключа id_user.
     */
    private Long idUser;

    /**
     * Составная часть ключа id_role.
     */
    private Long idRole;
}
