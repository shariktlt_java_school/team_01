package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class StudentGroup {
    /**
     * Составная часть ключа user_id.
     */
    private Long userId;

    /**
     * Составная часть ключа group_id.
     */
    private Long groupId;
}
