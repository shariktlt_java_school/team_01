package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class TaskLesson {
    /**
     * Составная часть ключа id_task.
     */
    private Long idTask;

    /**
     * Составная часть ключа id_lesson.
     */
    private Long idLesson;
}
