package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.da.GroupDALayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("GroupServiceLayer")
public class GroupServiceLayer implements GroupService {

    /**
     * Зависимость для слоя доступа к данным групп.
     */
    @Autowired
    private GroupDALayer daLayer;

    /**
     * Получение информации о группе.
     *
     * @param id
     * @return информация о группе
     */
    @Override
    public Group getGroup(final long id) {
        return daLayer.getGroup(id);
    }

    /**
     * Получение групп, к которым принадлежит студент.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Group> getGroupsByStudentId(final long id) {
        return daLayer.getGroupsByStudentId(id);
    }

    /**
     * Получение id студентов, которые принадлежат к группе.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Long> getStudentsByGroupId(final long id) {
        return daLayer.getStudentsByGroupId(id);
    }

    /**
     * Получение всех групп.
     *
     * @return список
     */
    @Override
    public List<Group> getAllGroups() {
        return daLayer.getAllGroups();
    }

    /**
     * Создание новой группы.
     *
     * @param groupForm
     * @return запись
     */
    @Override
    public Group createGroup(final GroupForm groupForm) {
        Group draft = Group.builder()
                .title(groupForm.getTitle())
                .build();
        draft = daLayer.create(draft);

        this.linkGroup(draft.getId(), groupForm.getStudents());

        return draft;
    }

    private void linkGroup(final Long groupId, final List<Long> ids) {
        ids.forEach(id -> daLayer.linkGroup(id, groupId));
    }
}
