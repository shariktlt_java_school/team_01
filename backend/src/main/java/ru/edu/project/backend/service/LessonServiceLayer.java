package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonForm;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.da.LessonDALayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("LessonServiceLayer")
public class LessonServiceLayer  implements LessonService {

    /**
     * Зависимость для слоя доступа к данным групп.
     */
    @Autowired
    private LessonDALayer daLayer;
    /**
     * Получение информации о занятии.
     *
     * @param id
     * @return Lesson
     */
    @Override
    public Lesson getLesson(final long id) {
        return daLayer.getLesson(id);
    }

    /**
     * Получение всех занятий по группе.
     *
     * @param id
     * @return информация о группе
     */
    @Override
    public List<Lesson> getLessonsByGroupId(final long id) {
        return daLayer.getLessonsByGroupId(id);
    }

    /**
     * Получение занятий по id пользователя(учителя).
     *
     * @param id
     * @return
     */
    @Override
    public List<Lesson> getLessonsByUserId(final long id) {
        return daLayer.getLessonsByUserId(id);
    }

    /**
     * Получение информации по занятиям по списку занятий.
     *
     * @param ids
     * @return
     */
    @Override
    public List<Lesson> getLessonsInfoByIds(final List<Long> ids) {
        return daLayer.getLessonsInfoByIds(ids);
    }

    /**
     * Получение id учителей по id занятия.
     *
     * @param id
     * @return
     */
    @Override
    public List<Long> getTeachersIdByLessonId(final long id) {
        return daLayer.getTeachersIdByLessonId(id);
    }

    /**
     * Создание занятия.
     *
     * @param lessonForm
     * @return lesson
     */
    @Override
    public Lesson createLesson(final LessonForm lessonForm) {
        Lesson draft = Lesson.builder()
                .classRoom(lessonForm.getClassRoom())
                .startTime(lessonForm.getStartTime())
                .endTime(lessonForm.getEndTime())
                .build();
        draft = daLayer.createLesson(draft);

        this.linkTeacher(lessonForm.getIdTeacher(), draft.getId());
        this.linkGroup(draft.getId(), lessonForm.getIdGroups());

        return draft;
    }

    /**
     * Получение списка групп, которые связаны с уроком.
     *
     * @param id
     * @return list
     */
    @Override
    public List<Long> getGroupsIdsByLessonId(final long id) {
        return daLayer.getGroupsIdsByLessonId(id);
    }

    private void linkTeacher(final Long teacherId, final Long lessonId) {
        daLayer.linkTeacher(teacherId, lessonId);
    }

    private void linkGroup(final Long lessonId, final List<Long> ids) {
        ids.forEach(id -> daLayer.linkGroups(lessonId, id));
    }

}
