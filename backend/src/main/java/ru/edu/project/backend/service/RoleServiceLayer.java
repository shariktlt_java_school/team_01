package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.da.RoleDALayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("RoleServiceLayer")
public class RoleServiceLayer implements RoleService {

    /**
     * Зависимость для слоя доступа к данным ролей.
     */
    @Autowired
    private RoleDALayer daLayer;

    /**
     * Получение информации о роли по ее коду.
     *
     * @param code
     * @return Role
     */
    @Override
    public Role getRole(final EnumRoles code) {
        return daLayer.getRole(code.getCode());
    }

    /**
     * Получение всех ролей.
     *
     * @return список
     */
    @Override
    public List<Role> getRoles() {
        return daLayer.getRoles();
    }

    /**
     * Получение роли пользователя.
     *
     * @param id
     * @return Role
     */
    @Override
    public Role getRoleByUserId(final long id) {
        return daLayer.getRoleByUserId(id);
    }

    /**
     * Получение пользователей по роли.
     *
     * @param code
     */
    @Override
    public List<Long> getUsersByRole(final EnumRoles code) {
        return daLayer.getUsersByRole(code.getCode());
    }
}
