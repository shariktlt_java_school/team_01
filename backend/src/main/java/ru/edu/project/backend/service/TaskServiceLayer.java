package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.da.TaskDALayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("TaskServiceLayer")
public class TaskServiceLayer implements TaskService {

    /**
     * Зависимость для слоя доступа к данным пользователей.
     */
    @Autowired
    private TaskDALayer daLayer;

    /**
     * Получение информации по заданию.
     *
     * @param id
     * @return задание
     */
    @Override
    public Task getTask(final long id) {
        return daLayer.getTask(id);
    }

    /**
     * Получение списка занятий к заданию.
     *
     * @param id
     * @return list
     */
    @Override
    public List<Long> getLessonsIdByTaskId(final long id) {
        return daLayer.getLessonsIdByTaskId(id);
    }

    /**
     * Получение заданий по списку заданий.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Task> getTasks(final List<Long> ids) {
        return daLayer.getTasks(ids);
    }

    /**
     * Получение всех заданий.
     *
     * @return список
     */
    @Override
    public List<Task> getAllTasks() {
        return daLayer.getAllTasks();
    }

    /**
     * Получение всех заданий по занятию.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Task> getTasksByLessonId(final long id) {
        return daLayer.getTasksByLessonId(id);
    }

    /**
     * Получение всех заданий по занятиям.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<Task> getTasksByLessonsId(final List<Long> ids) {
        return daLayer.getTasksByLessonsId(ids);
    }

    /**
     * Получение id всех сданных заданий студентом.
     *
     * @param id
     * @return список
     */
    @Override
    public List<Long> getProgressTasksIdsByStudentId(final long id) {
        return daLayer.getProgressTasksIdsByStudentId(id);
    }

    /**
     * Получение решения по id студента и id задания.
     *
     * @param studentId
     * @param taskId
     * @return
     */
    @Override
    public StudentProgress getProgress(final long studentId, final long taskId) {
        return daLayer.getProgress(studentId, taskId);
    }

    /**
     * Получение всех решений студентом.
     *
     * @param id
     * @return
     */
    @Override
    public List<StudentProgress> getProgressByStudentId(final long id) {
        return daLayer.getProgressByStudentId(id);
    }

    /**
     *
     * @param taskForm
     * @return lesson
     */
    @Override
    public Task createTask(final TaskForm taskForm) {
        Task draft = Task.builder()
                .name(taskForm.getName())
                .description(taskForm.getDescription())
                .build();

        draft = daLayer.createTask(draft);

        this.linkLesson(draft.getId(), taskForm.getIdsLessons());

        return draft;
    }

    /**
     * Создание ответа студента.
     *
     * @param progress
     * @return progress
     */
    @Override
    public StudentProgress createProgress(final StudentProgress progress) {
        return daLayer.saveProgress(progress);
    }

    /**
     * Редактирование ответа студента.
     *
     * @param progress
     * @return progress
     */
    @Override
    public StudentProgress updateProgress(final StudentProgress progress) {
        StudentProgress progressSync = daLayer.getProgress(progress.getUserId(), progress.getTaskId());
        progressSync.setVerified(progress.getVerified());
        progressSync.setAssessment(progress.getAssessment());

        return daLayer.saveProgress(progressSync);
    }

    private void linkLesson(final Long taskId, final List<Long> ids) {
        ids.forEach(id -> daLayer.linkLesson(taskId, id));
    }
}
