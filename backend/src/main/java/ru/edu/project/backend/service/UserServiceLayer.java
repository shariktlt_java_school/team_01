package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserForm;
import ru.edu.project.backend.api.users.UserService;
import ru.edu.project.backend.da.RoleDALayer;
import ru.edu.project.backend.da.UserDALayer;

import java.util.List;

import static java.util.Optional.ofNullable;

@Service
@Profile("!STUB")
@Qualifier("UserServiceLayer")
public class UserServiceLayer implements UserService {
    /**
     * Пустой пользователь.
     */
    public static final User EMPTY_USER = User.builder().id(-1L).build();

    /**
     * Зависимость для слоя доступа к данным пользователей.
     */
    @Autowired
    private UserDALayer daLayer;

    /**
     * Зависимость для слоя доступа к ролям пользователей.
     */
    @Autowired
    private RoleDALayer daRoleLayer;

    /**
     *
     * @param userForm
     * @return long
     */
    @Override
    public Long register(final UserForm userForm) {
        User draft = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .patronymic(userForm.getPatronymic())
                .birthday(userForm.getBirthDay())
                .login(userForm.getLogin())
                .password(userForm.getPassword())
                .build();

        draft = daLayer.createUser(draft);


        if (draft == null) {
            throw new IllegalStateException("не удалось зарегистрироваться");
        }
        daRoleLayer.linkRole(draft.getId(), userForm.getRole());
        return draft.getId();
    }

    /**
     * Поиск пользователя по логину.
     *
     * @param login
     * @return user
     */
    @Override
    public User loadUserByLogin(final String login) {
        return ofNullable(daLayer.loadUserByLogin(login)).orElse(EMPTY_USER);
    }

    /**
     * Возвращает данные о пользователе.
     *
     * @param id
     * @return пользователь
     */
    @Override
    public User getUser(final long id) {
        return daLayer.getUser(id);
    }

    /**
     * Возвращает список пользователей по списку id.
     *
     * @param ids
     * @return список
     */
    @Override
    public List<User> getUsers(final List<Long> ids) {
        return daLayer.getUsers(ids);
    }

    /**
     * Регистрация нового пользователя.
     *
     * @param userForm
     * @return запись
     */
    @Override
    public User createUser(final UserForm userForm) {

        User draft = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .patronymic(userForm.getPatronymic())
                .birthday(userForm.getBirthDay())
                .login(userForm.getLogin())
                .password(userForm.getPassword())
                .build();

        draft = daLayer.createUser(draft);

        daRoleLayer.linkRole(draft.getId(), userForm.getRole());

        return draft;
    }
}
