package ru.edu.project.backend.app;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IndexControllerTest {

    @Test
    void index() {
        IndexController indexController = new IndexController();
        assertEquals("index", indexController.index());
    }
}