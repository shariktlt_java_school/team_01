package ru.edu.project.backend.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.service.GroupServiceLayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class GroupControllerTest {
    public static final long ID = 1L;

    @Mock
    private GroupServiceLayer delegate;

    @InjectMocks
    private GroupController groupController;

    @Mock
    private Group group;

    @BeforeEach
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    void getGroup() {
        when(delegate.getGroup(ID)).thenReturn(group);

        assertEquals(group, groupController.getGroup(ID));
        verify(delegate).getGroup(ID);
    }

    @Test
    void getGroupsByStudentId() {
        List<Group> groups = new ArrayList<>();
        groups.add(group);

        when(delegate.getGroupsByStudentId(ID)).thenReturn(groups);

        assertEquals(groups, groupController.getGroupsByStudentId(ID));
        verify(delegate).getGroupsByStudentId(ID);
    }

    @Test
    void getStudentsByGroupId() {
        List<Long> groups = new ArrayList<>();
        groups.add(1L);

        when(delegate.getStudentsByGroupId(ID)).thenReturn(groups);

        assertEquals(groups, groupController.getStudentsByGroupId(ID));
        verify(delegate).getStudentsByGroupId(ID);
    }

    @Test
    void getAllGroups() {
        List<Group> groups = new ArrayList<>();
        groups.add(group);

        when(delegate.getAllGroups()).thenReturn(groups);

        assertEquals(groups, groupController.getAllGroups());
        verify(delegate).getAllGroups();
    }

    @Test
    void createGroup() {
        GroupForm groupForm = mock(GroupForm.class);
        when(delegate.createGroup(groupForm)).thenReturn(group);

        assertEquals(group, groupController.createGroup(groupForm));
        verify(delegate).createGroup(groupForm);
    }
}