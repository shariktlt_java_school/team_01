package ru.edu.project.backend.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.service.RoleServiceLayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class RoleControllerTest {

    @Mock
    private RoleServiceLayer delegate;

    @InjectMocks
    private RoleController roleController;

    @Mock
    Role role;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getRole() {
        when(delegate.getRole(EnumRoles.STUDENT)).thenReturn(role);

        assertEquals(role, roleController.getRole(EnumRoles.STUDENT));
        verify(delegate).getRole(EnumRoles.STUDENT);
    }

    @Test
    void getRoles() {
        List<Role> roles = new ArrayList<>();
        when(delegate.getRoles()).thenReturn(roles);

        assertEquals(roles, roleController.getRoles());
        verify(delegate).getRoles();
    }

    @Test
    void getRoleByUserId() {
        when(delegate.getRoleByUserId(1L)).thenReturn(role);

        assertEquals(role, roleController.getRoleByUserId(1L));
        verify(delegate).getRoleByUserId(1L);
    }

    @Test
    void getUsersByRole() {
        List<Long> longs = new ArrayList<>();

        when(delegate.getUsersByRole(EnumRoles.STUDENT)).thenReturn(longs);

        assertEquals(longs, roleController.getUsersByRole(EnumRoles.STUDENT));
        verify(delegate).getUsersByRole(EnumRoles.STUDENT);
    }
}