package ru.edu.project.backend.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.service.TaskServiceLayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class TaskControllerTest {

    @Mock
    private TaskServiceLayer delegate;

    @InjectMocks
    private TaskController taskController;

    @Mock
    private Task task;

    @Mock
    private StudentProgress progress;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getTask() {
        when(delegate.getTask(1L)).thenReturn(task);

        assertEquals(task, taskController.getTask(1L));
        verify(delegate).getTask(1L);
    }

    @Test
    void getLessonsIdByTaskId() {
        List<Long> longs = new ArrayList<>();

        when(delegate.getLessonsIdByTaskId(1L)).thenReturn(longs);

        assertEquals(longs, taskController.getLessonsIdByTaskId(1L));
        verify(delegate).getLessonsIdByTaskId(1L);
    }

    @Test
    void getTasks() {
        List<Long> longs = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        when(delegate.getTasks(longs)).thenReturn(tasks);

        assertEquals(tasks, taskController.getTasks(longs));
        verify(delegate).getTasks(longs);
    }

    @Test
    void getAllTasks() {
        List<Task> tasks = new ArrayList<>();

        when(delegate.getAllTasks()).thenReturn(tasks);

        assertEquals(tasks, taskController.getAllTasks());
        verify(delegate).getAllTasks();
    }

    @Test
    void getTasksByLessonId() {
        List<Task> tasks = new ArrayList<>();

        when(delegate.getTasksByLessonId(1L)).thenReturn(tasks);

        assertEquals(tasks, taskController.getTasksByLessonId(1L));
        verify(delegate).getTasksByLessonId(1L);
    }

    @Test
    void getTasksByLessonsId() {
        List<Long> longs = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        when(delegate.getTasksByLessonsId(longs)).thenReturn(tasks);

        assertEquals(tasks, taskController.getTasksByLessonsId(longs));
        verify(delegate).getTasksByLessonsId(longs);
    }

    @Test
    void getProgressTasksIdsByStudentId() {
        List<Long> longs = new ArrayList<>();

        when(delegate.getProgressTasksIdsByStudentId(1L)).thenReturn(longs);

        assertEquals(longs, taskController.getProgressTasksIdsByStudentId(1L));
        verify(delegate).getProgressTasksIdsByStudentId(1L);
    }

    @Test
    void getProgress() {
        when(delegate.getProgress(1L, 1L)).thenReturn(progress);

        assertEquals(progress, taskController.getProgress(1L, 1L));
        verify(delegate).getProgress(1L, 1L);
    }

    @Test
    void getProgressByStudentId() {
        List<StudentProgress> studentProgresses = new ArrayList<>();

        when(delegate.getProgressByStudentId(1L)).thenReturn(studentProgresses);

        assertEquals(studentProgresses, taskController.getProgressByStudentId(1L));
        verify(delegate).getProgressByStudentId(1L);
    }

    @Test
    void createTask() {
        TaskForm taskForm = mock(TaskForm.class);

        when(delegate.createTask(taskForm)).thenReturn(task);

        assertEquals(task, taskController.createTask(taskForm));
        verify(delegate).createTask(taskForm);
    }

    @Test
    void createProgress() {
        when(delegate.createProgress(progress)).thenReturn(progress);

        assertEquals(progress, taskController.createProgress(progress));
        verify(delegate).createProgress(progress);
    }

    @Test
    void updateProgress() {
        when(delegate.updateProgress(progress)).thenReturn(progress);

        assertEquals(progress, taskController.updateProgress(progress));
        verify(delegate).updateProgress(progress);
    }
}