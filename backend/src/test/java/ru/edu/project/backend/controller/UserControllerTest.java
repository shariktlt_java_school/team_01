package ru.edu.project.backend.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserForm;
import ru.edu.project.backend.service.UserServiceLayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class UserControllerTest {
    @Mock
    private UserServiceLayer delegate;

    @Mock
    private User user;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void register() {
        UserForm userForm = mock(UserForm.class);

        when(delegate.register(userForm)).thenReturn(1L);

        assertEquals(1L, userController.register(userForm));
        verify(delegate).register(userForm);
    }

    @Test
    void loadUserByLogin() {
        when(delegate.loadUserByLogin("login")).thenReturn(user);

        assertEquals(user, userController.loadUserByLogin("login"));
        verify(delegate).loadUserByLogin("login");
    }

    @Test
    void getUser() {
        when(delegate.getUser(1L)).thenReturn(user);

        assertEquals(user, userController.getUser(1L));
        verify(delegate).getUser(1L);
    }

    @Test
    void getUsers() {
        List<User> users = new ArrayList<>();
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(delegate.getUsers(longs)).thenReturn(users);

        assertEquals(users, userController.getUsers(longs));
        verify(delegate).getUsers(longs);
    }

    @Test
    void createUser() {
        UserForm userForm = mock(UserForm.class);

        when(delegate.createUser(userForm)).thenReturn(user);

        assertEquals(user, userController.createUser(userForm));
        verify(delegate).createUser(userForm);
    }
}