package ru.edu.project.backend.da.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.da.jpa.converter.group.GroupMapper;
import ru.edu.project.backend.da.jpa.converter.group.StudentGroupMapper;
import ru.edu.project.backend.da.jpa.entity.group.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.group.StudentGroupEntity;
import ru.edu.project.backend.da.jpa.repository.group.GroupEntityRepository;
import ru.edu.project.backend.da.jpa.repository.group.StudentGroupEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class JPAGroupDATest {

    public static final long ID = 1L;

    @Mock
    private GroupEntityRepository groupRepo;

    @Mock
    private StudentGroupEntityRepository studentGroupRepo;

    @Mock
    private StudentGroupMapper studentGroupMapper;

    @Mock
    private GroupMapper mapper;

    @InjectMocks
    private JPAGroupDA groupDA;

    @Mock
    private Group group;

    @Mock
    private GroupEntity groupEntity;

    @Mock
    private StudentGroupEntity studentGroupEntity;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void linkGroup() {
        groupDA.linkGroup(ID, ID);
    }

    @Test
    void getGroup() {
        when(group.getId()).thenReturn(ID);
        when(group.getTitle()).thenReturn("title");

        when(groupRepo.findById(ID)).thenReturn(Optional.of(groupEntity));
        when(mapper.map(groupEntity)).thenReturn(group);

        Group resultGroup = groupDA.getGroup(ID);

        verify(groupRepo, times(1)).findById(ID);
        assertEquals(group, resultGroup);

    }

    @Test
    void getGroupsByStudentId() {
        List<Group> groupList = new ArrayList<>();

        List<GroupEntity> list = new ArrayList<>();

        List<StudentGroupEntity> studentGroupEntities = new ArrayList<>();

        when(studentGroupRepo.findAllByPkUserId(ID)).thenReturn(studentGroupEntities);
        when(mapper.map(list)).thenReturn(groupList);

        assertEquals(groupList, groupDA.getGroupsByStudentId(ID));
    }

    @Test
    void getStudentsByGroupId() {
        List<StudentGroupEntity> groupStudentEntities = new ArrayList<>();
        List<Long> longs = new ArrayList<>();

        when(studentGroupRepo.findAllByPkGroupId(ID)).thenReturn(groupStudentEntities);
        assertEquals(longs, groupDA.getStudentsByGroupId(ID));
    }

    @Test
    void getAllGroups() {
        Iterable<GroupEntity> allGroups = new ArrayList<>();
        List<Group> list = new ArrayList<>();

        when(groupRepo.findAll()).thenReturn(allGroups);

        assertEquals(list, groupDA.getAllGroups());
    }

    @Test
    void create() {
        GroupEntity saved = mock(GroupEntity.class);

        when(mapper.map(group)).thenReturn(groupEntity);
        when(groupRepo.save(groupEntity)).thenReturn(saved);
        when(saved.getId()).thenReturn(ID);

        groupDA.create(group);

        verify(groupRepo, times(1)).save(groupEntity);

    }
}