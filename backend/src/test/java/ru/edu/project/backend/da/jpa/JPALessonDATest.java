package ru.edu.project.backend.da.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.da.jpa.converter.lesson.LessonGroupMapper;
import ru.edu.project.backend.da.jpa.converter.lesson.LessonMapper;
import ru.edu.project.backend.da.jpa.converter.lesson.LessonTeacherMapper;
import ru.edu.project.backend.da.jpa.entity.group.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.group.StudentGroupEntity;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonEntity;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonGroupEntity;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonTeacherEntity;
import ru.edu.project.backend.da.jpa.repository.lesson.LessonEntityRepository;
import ru.edu.project.backend.da.jpa.repository.lesson.LessonGroupEntityRepository;
import ru.edu.project.backend.da.jpa.repository.lesson.LessonTeacherEntityRepository;
import ru.edu.project.backend.model.LessonGroup;
import ru.edu.project.backend.model.LessonTeacher;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class JPALessonDATest {
    public static final long ID = 1L;
    @Mock
    private LessonEntityRepository repository;

    @Mock
    private LessonGroupEntityRepository lessonGroupRepository;

    @Mock
    private LessonTeacherEntityRepository lessonTeacherRepository;

    @Mock
    private LessonGroupMapper lessonGroupMapper;

    @Mock
    private LessonTeacherMapper lessonTeacherMapper;

    @Mock
    private LessonMapper mapper;

    @Mock
    private Lesson lesson;

    @Mock
    private LessonEntity lessonEntity;

    @Mock
    private LessonGroupEntity lessonGroupEntity;

    @Mock
    private LessonTeacherEntity lessonTeacherEntity;

    @InjectMocks
    private JPALessonDA jpaLessonDA;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getLesson() {
        when(repository.findById(ID)).thenReturn(Optional.of(lessonEntity));
        when(mapper.map(lessonEntity)).thenReturn(lesson);

        Lesson taskResult = jpaLessonDA.getLesson(ID);
        verify(repository, times(1)).findById(ID);
        assertEquals(lesson, taskResult);
    }

    @Test
    void getLessonsByGroupId() {
        List<Lesson> lessonList = new ArrayList<>();

        List<LessonEntity> list = new ArrayList<>();

        List<LessonGroupEntity> lessonGroupEntities = new ArrayList<>();

        when(lessonGroupRepository.findAllByPkIdGroup(ID)).thenReturn(lessonGroupEntities);
        when(mapper.map(list)).thenReturn(lessonList);

        assertEquals(lessonList, jpaLessonDA.getLessonsByGroupId(ID));

    }

    @Test
    void getLessonsByUserId() {
        List<Lesson> lessonList = new ArrayList<>();

        List<LessonEntity> list = new ArrayList<>();

        List<LessonTeacherEntity> lessonTeacherEntities = new ArrayList<>();

        when(lessonTeacherRepository.findAllByPkIdTeacher(ID)).thenReturn(lessonTeacherEntities);
        when(mapper.map(list)).thenReturn(lessonList);

        assertEquals(lessonList, jpaLessonDA.getLessonsByUserId(ID));
    }

    @Test
    void getLessonsInfoByIds() {
        List<Long> longs = new ArrayList<>();
        List<Lesson> lessonList = new ArrayList<>();
        List<LessonEntity> lessonEntities = new ArrayList<>();

        when(repository.findAllById(longs)).thenReturn(lessonEntities);
        when(mapper.map(lessonEntities)).thenReturn(lessonList);

        assertEquals(lessonList, jpaLessonDA.getLessonsInfoByIds(longs));
    }

    @Test
    void getTeachersIdByLessonId() {
        List<LessonTeacherEntity> entities = new ArrayList<>();
        List<LessonEntity> list = new ArrayList<>();
        List<Lesson> lessonList = new ArrayList<>();
        List<Long> result = new ArrayList<>();

        when(lessonTeacherRepository.findAllByPkIdLesson(ID)).thenReturn(entities);
        when(mapper.map(list)).thenReturn(lessonList);

        assertEquals(result, jpaLessonDA.getTeachersIdByLessonId(ID));
    }

    @Test
    void createLesson() {
        when(mapper.map(lesson)).thenReturn(lessonEntity);
        when(repository.save(lessonEntity)).thenReturn(lessonEntity);
        when(lessonEntity.getId()).thenReturn(ID);

        jpaLessonDA.createLesson(lesson);

        verify(repository, times(1)).save(lessonEntity);
    }

    @Test
    void linkTeacher() {
        jpaLessonDA.linkTeacher(ID, ID);
    }

    @Test
    void linkGroups() {
        jpaLessonDA.linkGroups(ID, ID);
    }

    @Test
    void getGroupsIdsByLessonId() {
        List<LessonGroupEntity> entities = new ArrayList<>();
        List<LessonGroup> groupList = new ArrayList<>();
        List<Long> result = new ArrayList<>();

        when(lessonGroupRepository.findAllByPkIdLesson(ID)).thenReturn(entities);
        when(lessonGroupMapper.map(entities)).thenReturn(groupList);

        assertEquals(result, jpaLessonDA.getGroupsIdsByLessonId(ID));
    }
}