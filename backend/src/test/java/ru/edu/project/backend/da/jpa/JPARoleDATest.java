package ru.edu.project.backend.da.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.da.jpa.converter.role.RoleMapper;
import ru.edu.project.backend.da.jpa.converter.role.RoleUserMapper;
import ru.edu.project.backend.da.jpa.entity.role.RoleEntity;
import ru.edu.project.backend.da.jpa.entity.role.RoleUserEntity;
import ru.edu.project.backend.da.jpa.repository.role.RoleEntityRepository;
import ru.edu.project.backend.da.jpa.repository.role.RoleUserEntityRepository;
import ru.edu.project.backend.model.RoleUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class JPARoleDATest {
    public static final long ID = 1L;

    @Mock
    private RoleEntityRepository repository;

    @Mock
    private RoleUserEntityRepository roleUserRepository;

    @Mock
    private RoleMapper mapper;

    @Mock
    private RoleUserMapper mapperRoleUser;

    @InjectMocks
    private JPARoleDA jpaRoleDA;

    @Mock
    private Role role;

    @Mock
    private RoleEntity roleEntity;

    @Mock
    private RoleUserEntity roleUserEntity;

    @Mock
    private RoleUser roleUser;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void linkRole() {
        jpaRoleDA.linkRole(ID, ID);
    }

    @Test
    void getRole() {
        Optional<RoleEntity> roleEntityOpt = Optional.empty();
        when(repository.findById(ID)).thenReturn(roleEntityOpt);
        when(mapper.map(roleEntity)).thenReturn(role);

        jpaRoleDA.getRole(ID);
        verify(repository, times(1)).findById(ID);
    }

    @Test
    void getRoles() {
        List<Role> roleList = new ArrayList<>();
        List<RoleEntity> entities = new ArrayList<>();

        when(repository.findAll()).thenReturn(entities);
        when(mapper.map(entities)).thenReturn(roleList);

        assertEquals(roleList, jpaRoleDA.getRoles());
        verify(repository, times(1)).findAll();
    }

    @Test
    void getRoleByUserId() {
        /*when(roleUserRepository.findFirstByPkIdRole(ID)).thenReturn(roleUserEntity);
        when(mapper.map(roleUserEntity.getRole())).thenReturn(role);

        assertEquals(role, jpaRoleDA.getRoleByUserId(ID));
        verify(roleUserRepository, times(1)).findFirstByPkIdRole(ID);*/
    }

    @Test
    void getUsersByRole() {
        List<RoleUserEntity> entity = new ArrayList<>();
        List<RoleUser> roleUsers = new ArrayList<>();
        List<Long> longs = new ArrayList<>();

        when(roleUserRepository.findAllByPkIdRole(ID)).thenReturn(entity);
        when(mapperRoleUser.map(entity)).thenReturn(roleUsers);
        when(roleUser.getIdUser()).thenReturn(1L);

        assertEquals(longs, jpaRoleDA.getUsersByRole(ID));
        verify(roleUserRepository, times(1)).findAllByPkIdRole(ID);
    }
}