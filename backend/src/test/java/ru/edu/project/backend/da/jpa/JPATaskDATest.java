package ru.edu.project.backend.da.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.da.jpa.converter.task.StudentProgressMapper;
import ru.edu.project.backend.da.jpa.converter.task.TaskLessonMapper;
import ru.edu.project.backend.da.jpa.converter.task.TaskMapper;
import ru.edu.project.backend.da.jpa.entity.task.StudentProgressEntity;
import ru.edu.project.backend.da.jpa.entity.task.TaskEntity;
import ru.edu.project.backend.da.jpa.entity.task.TaskLessonEntity;
import ru.edu.project.backend.da.jpa.repository.task.StudentProgressEntityRepository;
import ru.edu.project.backend.da.jpa.repository.task.TaskEntityRepository;
import ru.edu.project.backend.da.jpa.repository.task.TaskLessonEntityRepository;
import ru.edu.project.backend.model.TaskLesson;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class JPATaskDATest {
    public static final long ID = 1L;
    @Mock
    private TaskEntityRepository repository;

    @Mock
    private TaskLessonEntityRepository taskLessonRepository;

    @Mock
    private StudentProgressEntityRepository studentRepository;

    @Mock
    private TaskLessonMapper taskLessonMapper;

    @Mock
    private StudentProgressMapper progressMapper;

    @Mock
    private TaskMapper mapper;

    @Mock
    private TaskEntity taskEntity;

    @Mock
    private StudentProgress studentProgress;

    @Mock
    private StudentProgressEntity studentProgressEntity;

    @Mock
    private TaskLessonEntity taskLessonEntity;

    @Mock
    private Task task;

    @InjectMocks
    private JPATaskDA jpaTaskDA;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getTask() {
        when(repository.findById(ID)).thenReturn(Optional.of(taskEntity));
        when(mapper.map(taskEntity)).thenReturn(task);

        Task taskResult = jpaTaskDA.getTask(ID);
        verify(repository, times(1)).findById(ID);
        assertEquals(task, taskResult);
    }

    @Test
    void getTasks() {
        List<Long> longs = new ArrayList<>();
        Iterable<TaskEntity> taskEntities = new ArrayList<>();
        List<Task> taskList = new ArrayList<>();

        when(repository.findAllById(longs)).thenReturn(taskEntities);
        when(mapper.map(taskEntities)).thenReturn(taskList);

        assertEquals(taskList, jpaTaskDA.getTasks(longs));
    }

    @Test
    void getAllTasks() {
        Iterable<TaskEntity> taskEntities = new ArrayList<>();
        List<Task> taskList = new ArrayList<>();

        when(repository.findAll()).thenReturn(taskEntities);
        when(mapper.map(taskEntities)).thenReturn(taskList);

        assertEquals(taskList, jpaTaskDA.getAllTasks());
    }

    @Test
    void getTasksByLessonId() {
        List<Long> longs = new ArrayList<>();
        List<TaskLessonEntity> taskList = new ArrayList<>();
        List<TaskEntity> taskEntities = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        when(taskLessonRepository.findAllByPkIdLesson(ID)).thenReturn(taskList);
        when(mapper.map(taskEntities)).thenReturn(tasks);

        assertEquals(tasks, jpaTaskDA.getTasksByLessonId(ID));
    }

    @Test
    void getTasksByLessonsId() {
        List<Long> list = new ArrayList<>();
        List<TaskLessonEntity> entities = new ArrayList<>();
        List<TaskEntity> taskEntities = new ArrayList<>();
        List<Task> taskList = new ArrayList<>();

        when(taskLessonRepository.findAllByPkIdLesson(ID)).thenReturn(entities);
        when(mapper.map(taskEntities)).thenReturn(taskList);

        assertEquals(taskList, jpaTaskDA.getTasksByLessonsId(list));
    }

    @Test
    void getProgressTasksIdsByStudentId() {
        assertEquals(null, jpaTaskDA.getProgressTasksIdsByStudentId(ID));
    }

    @Test
    void getProgress() {
        when(studentRepository.findById(StudentProgressEntity.pk(ID, ID))).thenReturn(Optional.of(studentProgressEntity));
        when(progressMapper.map(studentProgressEntity)).thenReturn(studentProgress);

        StudentProgress studentProgress2 = jpaTaskDA.getProgress(ID, ID);

        assertEquals(null, studentProgress2);
    }

    @Test
    void getProgressByStudentId() {
        List<StudentProgressEntity> studentProgressEntities = new ArrayList<>();
        List<StudentProgress> studentProgresses = new ArrayList<>();

        when(studentRepository.findAllByPkIdStudent(ID)).thenReturn(studentProgressEntities);
        when(progressMapper.map(studentProgressEntities)).thenReturn(studentProgresses);

        assertEquals(studentProgresses, jpaTaskDA.getProgressByStudentId(ID));
    }

    @Test
    void createTask() {
        when(mapper.map(task)).thenReturn(taskEntity);
        when(repository.save(taskEntity)).thenReturn(taskEntity);
        when(taskEntity.getId()).thenReturn(ID);

        jpaTaskDA.createTask(task);

        verify(repository, times(1)).save(taskEntity);
    }

    @Test
    void linkLesson() {
        jpaTaskDA.linkLesson(ID, ID);
    }

    @Test
    void saveProgress() {
        when(progressMapper.map(studentProgress)).thenReturn(studentProgressEntity);
        when(studentRepository.save(studentProgressEntity)).thenReturn(studentProgressEntity);
        when(progressMapper.map(studentProgressEntity)).thenReturn(studentProgress);

        assertEquals(studentProgress, jpaTaskDA.saveProgress(studentProgress));
    }

    @Test
    void getLessonsIdByTaskId() {
        List<Long> longs = new ArrayList<>();
        List<TaskLessonEntity> taskEntities = new ArrayList<>();
        List<TaskLesson> taskLessons = new ArrayList<>();

        when(taskLessonRepository.findAllByPkIdTask(ID)).thenReturn(taskEntities);
        when(taskLessonMapper.map(taskEntities)).thenReturn(taskLessons);

        assertEquals(longs, jpaTaskDA.getLessonsIdByTaskId(ID));
    }
}