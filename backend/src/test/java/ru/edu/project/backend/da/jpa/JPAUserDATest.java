package ru.edu.project.backend.da.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.da.jpa.converter.user.UserMapper;
import ru.edu.project.backend.da.jpa.entity.group.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.user.UserEntity;
import ru.edu.project.backend.da.jpa.repository.user.UserEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class JPAUserDATest {

    public static final long ID = 1L;

    @InjectMocks
    private JPAUserDA jpaUserDA;

    @Mock
    private UserEntityRepository repository;

    @Mock
    private UserMapper mapper;

    @Mock
    private User user;

    @Mock
    UserEntity userEntity;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getUser() {
        jpaUserDA.getUser(ID);
        verify(repository, times(1)).findById(ID);
    }

    @Test
    void getUsers() {
        List<User> list = new ArrayList<>();
        List<Long> longs = new ArrayList<>();
        longs.add(1L);
        List<UserEntity> entities = new ArrayList<>();

        when(repository.findAllById(longs)).thenReturn(entities);
        when(mapper.map(entities)).thenReturn(list);

        assertEquals(list, jpaUserDA.getUsers(longs));
        verify(repository, times(1)).findAllById(longs);
    }

    @Test
    void createUser() {
        UserEntity saved = mock(UserEntity.class);

        when(mapper.map(user)).thenReturn(userEntity);
        when(repository.save(userEntity)).thenReturn(saved);
        when(saved.getId()).thenReturn(ID);

        assertEquals(user, jpaUserDA.createUser(user));
        verify(repository, times(1)).save(userEntity);
    }

    @Test
    void loadUserByLogin() {
        when(repository.findByLogin("login")).thenReturn(userEntity);
        when(mapper.map(userEntity)).thenReturn(user);
        when(user.getLogin()).thenReturn("login");

        assertEquals(user.getLogin(), jpaUserDA.loadUserByLogin("login").getLogin());

    }

}