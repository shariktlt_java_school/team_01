package ru.edu.project.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.da.GroupDALayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class GroupServiceLayerTest {
    @InjectMocks
    private GroupServiceLayer groupServiceLayer;

    @Mock
    private GroupDALayer daLayer;

    @Mock
    private Group group;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getGroup() {
        when(daLayer.getGroup(1L)).thenReturn(group);

        assertEquals(group, groupServiceLayer.getGroup(1L));
        verify(daLayer).getGroup(1L);
    }

    @Test
    void getGroupsByStudentId() {
        List<Group> groups = new ArrayList<>();
        groups.add(group);

        when(daLayer.getGroupsByStudentId(1L)).thenReturn(groups);

        assertEquals(groups, groupServiceLayer.getGroupsByStudentId(1L));
        verify(daLayer).getGroupsByStudentId(1L);
    }

    @Test
    void getStudentsByGroupId() {
        List<Long> groups = new ArrayList<>();
        groups.add(1L);

        when(daLayer.getStudentsByGroupId(1L)).thenReturn(groups);

        assertEquals(groups, groupServiceLayer.getStudentsByGroupId(1L));
        verify(daLayer).getStudentsByGroupId(1L);
    }

    @Test
    void getAllGroups() {
        List<Group> groups = new ArrayList<>();
        groups.add(group);

        when(daLayer.getAllGroups()).thenReturn(groups);

        assertEquals(groups, groupServiceLayer.getAllGroups());
        verify(daLayer).getAllGroups();
    }

    @Test
    void createGroup() {
        GroupForm groupForm = mock(GroupForm.class);
        Group resGroup = Group.builder()
                .id(1L)
                .title("title")
                .build();

        when(daLayer.create(group)).thenReturn(group);
        when(group.getId()).thenReturn(1L);

        when(daLayer.create(any(Group.class))).thenAnswer(invocationOnMock -> {
            Group info = invocationOnMock.getArgument(0, Group.class);
            assertEquals(groupForm.getTitle(), info.getTitle());
            return info;

        });
        groupServiceLayer.createGroup(groupForm);
    }
}