package ru.edu.project.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.da.RoleDALayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class RoleServiceLayerTest {
    @Mock
    private RoleDALayer daLayer;

    @InjectMocks
    private RoleServiceLayer roleServiceLayer;

    @Mock
    private Role role;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getRole() {
        when(daLayer.getRole(EnumRoles.ADMINISTRATOR.getCode())).thenReturn(role);

        assertEquals(role, roleServiceLayer.getRole(EnumRoles.ADMINISTRATOR));
        verify(daLayer).getRole(EnumRoles.ADMINISTRATOR.getCode());
    }

    @Test
    void getRoles() {
        List<Role> roles = new ArrayList<>();
        when(daLayer.getRoles()).thenReturn(roles);

        assertEquals(roles, roleServiceLayer.getRoles());
        verify(daLayer).getRoles();
    }

    @Test
    void getRoleByUserId() {
        when(daLayer.getRoleByUserId(1L)).thenReturn(role);

        assertEquals(role, roleServiceLayer.getRoleByUserId(1L));
        verify(daLayer).getRoleByUserId(1L);
    }

    @Test
    void getUsersByRole() {
        List<Long> longs = new ArrayList<>();

        when(daLayer.getUsersByRole(EnumRoles.STUDENT.getCode())).thenReturn(longs);

        assertEquals(longs, roleServiceLayer.getUsersByRole(EnumRoles.STUDENT));
        verify(daLayer).getUsersByRole(EnumRoles.STUDENT.getCode());
    }
}