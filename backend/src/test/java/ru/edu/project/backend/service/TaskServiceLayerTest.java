package ru.edu.project.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.da.TaskDALayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class TaskServiceLayerTest {

    @InjectMocks
    private TaskServiceLayer taskServiceLayer;

    @Mock
    private TaskDALayer daLayer;

    @Mock
    private Task task;

    @Mock
    private StudentProgress progress;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }
    @Test
    void getTask() {
        when(daLayer.getTask(1L)).thenReturn(task);

        assertEquals(task, taskServiceLayer.getTask(1L));
        verify(daLayer).getTask(1L);
    }

    @Test
    void getLessonsIdByTaskId() {
        List<Long> longs = new ArrayList<>();

        when(daLayer.getLessonsIdByTaskId(1L)).thenReturn(longs);

        assertEquals(longs, taskServiceLayer.getLessonsIdByTaskId(1L));
        verify(daLayer).getLessonsIdByTaskId(1L);
    }

    @Test
    void getTasks() {
        List<Long> longs = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        when(daLayer.getTasks(longs)).thenReturn(tasks);

        assertEquals(tasks, taskServiceLayer.getTasks(longs));
        verify(daLayer).getTasks(longs);
    }

    @Test
    void getAllTasks() {
        List<Task> tasks = new ArrayList<>();

        when(daLayer.getAllTasks()).thenReturn(tasks);

        assertEquals(tasks, taskServiceLayer.getAllTasks());
        verify(daLayer).getAllTasks();
    }

    @Test
    void getTasksByLessonId() {
        List<Task> tasks = new ArrayList<>();

        when(daLayer.getTasksByLessonId(1L)).thenReturn(tasks);

        assertEquals(tasks, taskServiceLayer.getTasksByLessonId(1L));
        verify(daLayer).getTasksByLessonId(1L);
    }

    @Test
    void getTasksByLessonsId() {
        List<Long> longs = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        when(daLayer.getTasksByLessonsId(longs)).thenReturn(tasks);

        assertEquals(tasks, taskServiceLayer.getTasksByLessonsId(longs));
        verify(daLayer).getTasksByLessonsId(longs);
    }

    @Test
    void getProgressTasksIdsByStudentId() {
        List<Long> longs = new ArrayList<>();

        when(daLayer.getProgressTasksIdsByStudentId(1L)).thenReturn(longs);

        assertEquals(longs, taskServiceLayer.getProgressTasksIdsByStudentId(1L));
        verify(daLayer).getProgressTasksIdsByStudentId(1L);
    }

    @Test
    void getProgress() {
        when(daLayer.getProgress(1L, 1L)).thenReturn(progress);

        assertEquals(progress, taskServiceLayer.getProgress(1L, 1L));
        verify(daLayer).getProgress(1L, 1L);
    }

    @Test
    void getProgressByStudentId() {
        List<StudentProgress> studentProgresses = new ArrayList<>();

        when(daLayer.getProgressByStudentId(1L)).thenReturn(studentProgresses);

        assertEquals(studentProgresses, taskServiceLayer.getProgressByStudentId(1L));
        verify(daLayer).getProgressByStudentId(1L);
    }

    @Test
    void createTask() {
        TaskForm taskForm = mock(TaskForm.class);

        when(daLayer.createTask(any(Task.class))).thenAnswer(invocationOnMock -> {
            Task taskInfo = invocationOnMock.getArgument(0, Task.class);
            return taskInfo;
        });
        taskServiceLayer.createTask(taskForm);
    }

    @Test
    void createProgress() {

        taskServiceLayer.createProgress(progress);
        verify(daLayer).saveProgress(progress);
    }

    @Test
    void updateProgress() {
        taskServiceLayer.createProgress(progress);
        verify(daLayer).saveProgress(progress);
    }
}