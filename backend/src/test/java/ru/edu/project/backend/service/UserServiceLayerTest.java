package ru.edu.project.backend.service;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserForm;
import ru.edu.project.backend.da.RoleDALayer;
import ru.edu.project.backend.da.UserDALayer;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class UserServiceLayerTest {
    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    @Mock
    private UserDALayer daLayer;

    @Mock
    private RoleDALayer daRoleLayer;

    @InjectMocks
    private UserServiceLayer userServiceLayer;

    @Mock
    private User user;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    @SneakyThrows
    void register() {
        String login = "login";
        String firstName = "firstName";
        String lastName = "lastname";
        String patronymic = "patronymic";
        String birthDay = "2000-10-10";
        String password = "password";
        Long role = 1L;

        Date parsed = FORMAT.parse(birthDay);
        Timestamp birthDayRes = new Timestamp(parsed.getTime());

        User draft = User.builder()
                .id(1L)
                .firstName(firstName)
                .lastName(lastName)
                .patronymic(patronymic)
                .birthday(birthDayRes)
                .login(login)
                .password(password)
                .build();

        UserForm userForm = UserForm.builder().id(1L).role(1L).build();

        when(daLayer.createUser(any(User.class))).thenAnswer(invocationOnMock -> {
            User info = invocationOnMock.getArgument(0, User.class);
            return draft;
        });

        userServiceLayer.register(userForm);
    }

    @Test
    @SneakyThrows
    void registerNull() {
        UserForm userForm = mock(UserForm.class);

        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> {
            userServiceLayer.register(userForm);
        });

    }

    @Test
    void loadUserByLogin() {
        when(daLayer.loadUserByLogin("login")).thenReturn(user);

        assertEquals(user, userServiceLayer.loadUserByLogin("login"));
        verify(daLayer).loadUserByLogin("login");
    }

    @Test
    void getUser() {
        when(daLayer.getUser(1L)).thenReturn(user);

        assertEquals(user, userServiceLayer.getUser(1L));
        verify(daLayer).getUser(1L);
    }

    @Test
    void getUsers() {
        List<User> users = new ArrayList<>();
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(daLayer.getUsers(longs)).thenReturn(users);

        assertEquals(users, userServiceLayer.getUsers(longs));
        verify(daLayer).getUsers(longs);
    }

    @Test
    @SneakyThrows
    void createUser() {
        String login = "login";
        String firstName = "firstName";
        String lastName = "lastname";
        String patronymic = "patronymic";
        String birthDay = "2000-10-10";
        String password = "password";
        Long role = 1L;

        Date parsed = FORMAT.parse(birthDay);
        Timestamp birthDayRes = new Timestamp(parsed.getTime());

        User draft = User.builder()
                .id(1L)
                .firstName(firstName)
                .lastName(lastName)
                .patronymic(patronymic)
                .birthday(birthDayRes)
                .login(login)
                .password(password)
                .build();

        UserForm userForm = UserForm.builder().id(1L).role(1L).build();

        when(daLayer.createUser(any(User.class))).thenAnswer(invocationOnMock -> {
            User info = invocationOnMock.getArgument(0, User.class);
            return draft;
        });

        userServiceLayer.createUser(userForm);

    }
}