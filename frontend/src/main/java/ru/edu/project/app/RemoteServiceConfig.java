package ru.edu.project.app;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;
import ru.edu.project.backend.RestServiceInvocationHandler;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.UserService;

import java.lang.reflect.Proxy;

@Configuration
@Profile("REST")
@SuppressWarnings("unchecked")
public class RemoteServiceConfig {

    /**
     * Создаем rest-прокси для GroupService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public GroupService groupService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/group");
        return getProxy(handler, GroupService.class);
    }

    /**
     * Создаем rest-прокси для LessonService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public LessonService lessonService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/lesson");
        return getProxy(handler, LessonService.class);
    }

    /**
     * Создаем rest-прокси для RoleService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public RoleService roleService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/role");
        return getProxy(handler, RoleService.class);
    }

    /**
     * Создаем rest-прокси для TaskService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public TaskService taskService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/task");
        return getProxy(handler, TaskService.class);
    }

    /**
     * Создаем rest-прокси для UserService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public UserService userService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/user");
        return getProxy(handler, UserService.class);
    }

    private <T> T getProxy(final RestServiceInvocationHandler handler, final Class<T>... tClass) {
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), tClass, handler);
    }

    /**
     * Создаем rest-прокси для RestTemplate.
     *
     * @return restTemplate
     */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RestTemplate restTemplateBean() {
        return new RestTemplate();
    }
}
