package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.UserService;
import ru.edu.project.backend.stub.groups.InMemoryStubGroupService;
import ru.edu.project.backend.stub.lessons.InMemoryStubLessonService;
import ru.edu.project.backend.stub.roles.InMemoryStubRoleService;
import ru.edu.project.backend.stub.tasks.InMemoryStubTaskService;
import ru.edu.project.backend.stub.users.InMemoryStubUserService;

@Configuration
@Profile("STUBS")
public class StubsConfig {

    /**
     * Заглушка сервиса групп.
     *
     * @return bean
     */
    @Bean
    public GroupService groupServiceBean() {
        return new InMemoryStubGroupService();
    }

    /**
     * Заглушка сервиса занятий.
     *
     * @return bean
     */
    @Bean
    public LessonService lessonServiceBean() {
        return new InMemoryStubLessonService();
    }

    /**
     * Заглушка сервиса ролей.
     *
     * @return bean
     */
    @Bean
    public RoleService roleServiceBean() {
        return new InMemoryStubRoleService();
    }

    /**
     * Заглушка сервиса заданий.
     *
     * @return bean
     */
    @Bean
    public TaskService taskServiceBean() {
        return new InMemoryStubTaskService();
    }

    /**
     * Заглушка сервиса пользователей.
     *
     * @return bean
     */
    @Bean
    public UserService userServiceBean() {
        return new InMemoryStubUserService();
    }
}
