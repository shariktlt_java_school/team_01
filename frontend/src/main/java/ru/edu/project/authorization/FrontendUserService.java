package ru.edu.project.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.roles.Role;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

@Service
public class FrontendUserService implements UserDetailsService {

    /**
     * Зависимость на бэкенд сервис.
     */
    @Autowired
    private UserService userService;

    /**
     * Зависимость на бэкенд сервис.
     */
    @Autowired
    private RoleService roleService;

    /**
     * Реализация метода поиска информации о пользователе.
     *
     * @param username
     * @return userDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        User user = userService.loadUserByLogin(username);

        if (user.getId() < 0) {
            throw new UsernameNotFoundException("user not found");
        }
        Role role = roleService.getRoleByUserId(user.getId());

        return new UserDetailsId(
                user.getId(),
                user.getLogin(),
                user.getPassword(),
                role.getName()
        );
    }
}
