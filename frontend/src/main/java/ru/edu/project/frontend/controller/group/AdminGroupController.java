package ru.edu.project.frontend.controller.group;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequestMapping("/user/administrator/group/")
@Controller
public class AdminGroupController {

    /**
     * Атрибут модели для хранения списка ошибок.
     */
    public static final String FORM_ERROR_ATTR = "errorsList";

    /**
     * Атрибут модели для хранения студентов.
     */
    public static final String STUDENTS = "students";

    /**
     * Параметр для ссылки.
     */
    public static final String ADMIN = "administrator";

    /**
     * Атрибут модели для хранения информации о пути в url.
     */
    public static final String PATH = "path";

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис ролей.
     */
    @Autowired
    private RoleService roleService;

    /**
     * Ссылка на сервис пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Отображение формы для создания группы.
     *
     * @param model
     * @return modemodelAndViewl
     */
    @GetMapping("/create")
    public String createForm(final Model model) {
        List<User> users = userService.getUsers(roleService.getUsersByRole(EnumRoles.STUDENT));
        model.addAttribute(STUDENTS, users);
        model.addAttribute(PATH, ADMIN);

        return "group/admin/create";
    }

    /**
     * Получаем форму с предварительной валидацией.
     *
     * @param form
     * @param bindingResult результат валидации формы
     * @param model
     * @return redirect url
     */
    @PostMapping("/create")
    public String createFormProcessing(
            @Valid
            @ModelAttribute final CreateForm form,
            final BindingResult bindingResult,
            final Model model
    ) {
        if (bindingResult.hasErrors()) {

            model.addAttribute(
                    FORM_ERROR_ATTR,
                    bindingResult.getAllErrors()
            );

            return createForm(model);
        }

        Group request = groupService.createGroup(GroupForm.builder()
                .students(form.students)
                .title(form.getTitle())
                .build());

        return "redirect:/user/administrator/";
    }

    @Getter
    @Setter
    public static class CreateForm {

        /**
         * Выбранные студенты.
         */
        private List<Long> students;

        /**
         * Наименование группы.
         */
        @NotNull
        private String title;

    }
}
