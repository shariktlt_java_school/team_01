package ru.edu.project.frontend.controller.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestMapping("/user/teacher/group/")
@Controller
public class TeacherGroupController {

    /**
     * Атрибут модели для хранения информации о группе.
     */
    public static final String GROUP = "group";

    /**
     * Атрибут модели для хранения информации о занятиях группы.
     */
    public static final String LESSONS = "lessons";

    /**
     * Атрибут модели для хранения информации о студентах в группе.
     */
    public static final String USERS = "users";

    /**
     * Атрибут модели для хранения информации о сданных заданиях и оценках по ним.
     */
    public static final String STUDENT_PROGRESS = "studentProgress";

    /**
     * Атрибут модели для хранения информации о занятиях и учителях.
     */
    public static final String LESSON_TEACHERS = "lessonTeachers";

    /**
     * Параметр для ссылки.
     */
    public static final String TEACHER = "teacher";

    /**
     * Атрибут модели для хранения информации о пути в url.
     */
    public static final String PATH = "path";

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис занятий.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Ссылка на сервис пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Просмотр группы по id.
     *
     * @param id
     * @param auth
     * @return ModelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView index(final @PathVariable("id") Long id, final Authentication auth) {
        long userId = getUserId(auth);

        ModelAndView model = new ModelAndView("group/teacher/view");

        Group groupInfo = groupService.getGroup(id);

        if (groupInfo == null) {
            model.addObject(PATH, TEACHER);
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("group/viewNotFound");
            return model;
        }

        List<Lesson> lessons = lessonService.getLessonsByGroupId(id);

        Map<Lesson, List<Task>> lessonTasks = getLessonListMap(lessons);

        Map<Lesson, List<User>> lessonTeachers = getTeachersByLesson(lessons);

        List<User> users = userService.getUsers(groupService.getStudentsByGroupId(id));

        Map<Long, StudentProgress> progressByTaskId = getMapTaskIdStudentProgress(userId);

        model.addObject(GROUP, groupInfo);
        model.addObject(LESSONS, lessonTasks);
        model.addObject(LESSON_TEACHERS, lessonTeachers);
        model.addObject(STUDENT_PROGRESS, progressByTaskId);
        model.addObject(USERS, users);
        model.addObject(GROUP, groupInfo);
        return model;
    }


    private Map<Lesson, List<User>> getTeachersByLesson(final List<Lesson> lessons) {
        if (lessons != null) {
            Map<Lesson, List<User>> result = new HashMap<>();
            lessons.forEach(lesson -> {
                List<User> users = userService.getUsers(lessonService.getTeachersIdByLessonId(lesson.getId()));
                result.put(lesson, users);
            });
            return result;
        }
        return null;
    }

    /**
     * Формирует мапу id заданий и прогресс студента по этому заданию.
     *
     * @param userId
     * @return map
     */
    private Map<Long, StudentProgress> getMapTaskIdStudentProgress(final long userId) {
        List<StudentProgress> studentProgress = taskService.getProgressByStudentId(userId);
        Map<Long, StudentProgress> progressByTaskId = studentProgress.stream().collect(Collectors.toMap(StudentProgress::getTaskId, Function.identity()));
        return progressByTaskId;
    }

    /**
     * Формирует мапу занятий и список заданий к ним.
     *
     * @param lessons
     * @return map
     */
    private Map<Lesson, List<Task>> getLessonListMap(final List<Lesson> lessons) {
        if (lessons != null) {
            Map<Lesson, List<Task>> lessonTasks = new HashMap<>();
            lessons.forEach(lesson -> {
                lessonTasks.put(lesson, taskService.getTasksByLessonId(lesson.getId()));
            });
            lessonTasks.entrySet().stream().sorted((o1, o2) -> o2.getKey().getStartTime().compareTo(o1.getKey().getStartTime()));
            return lessonTasks;
        }
        return null;
    }

    /**
     * Получаем userId.
     *
     * @param auth
     * @return userId
     */
    private long getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof UserDetailsId) {
            return ((UserDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }
}
