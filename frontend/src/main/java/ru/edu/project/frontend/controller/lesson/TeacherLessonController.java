package ru.edu.project.frontend.controller.lesson;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonForm;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RequestMapping("user/teacher/lesson")
@Controller
public class TeacherLessonController {
    /**
     * Атрибут модели для хранения списка ошибок.
     */
    public static final String FORM_ERROR_ATTR = "errorsList";


    /**
     * Параметр для ссылки.
     */
    public static final String TEACHER = "teacher";

    /**
     * Атрибут модели для хранения информации о группах.
     */
    public static final String GROUPS = "groups";


    /**
     * Служба получения пользователя.
     */
    @Autowired
    private UserService userService;
    /**
     * Служба получения групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Служба получения занятий.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Страница с формой добавления занятия.
     *
     * @param model
     * @param authentication
     * @return view
     */
    @GetMapping("/create")
    public String createForm(final Model model,
                             final Authentication authentication) {
        List<Group> groups = groupService.getAllGroups();
        model.addAttribute(GROUPS, groups);

        final long userId = ((UserDetailsId) authentication.getPrincipal()).getUserId();
        User teacher = userService.getUser(userId);
        model.addAttribute(TEACHER, teacher);

        return "lesson/create_les";
    }

    /**
     * Получние формы с предварительной валидацией.
     *
     * @param form
     * @param bindingResult
     * @param model
     * @param authentication
     * @return url
     */
    @PostMapping("/createLesson")
    public String createFormProcessing(
            @Valid
            @ModelAttribute final CreateForm form,
            final BindingResult bindingResult,
            final Model model, final Authentication authentication
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(
                    FORM_ERROR_ATTR,
                    bindingResult.getAllErrors()
            );
            return createForm(model, authentication);
        }

        final long userId = ((UserDetailsId) authentication.getPrincipal()).getUserId();

        Lesson request = lessonService.createLesson(LessonForm.builder()
                .classRoom(form.classRoom)
                .startTime(form.getStartTime())
                .endTime(form.getEndTime())
                .idGroups(form.idGroups)
                .idTeacher(userId)
                .build());

        return "redirect:/user/teacher/";
    }

    /**
     * CreateForm.
     */
    @Getter
    @Setter
    public static class CreateForm {

        /**
         * Для парсинга даты.
         */
        private static final DateFormat FORMAT;

        /**
         * id лекции.
         */
        private long id;

        /**
         * номер классной комнаты.
         */
        private int classRoom;

        /**
         * Время начала занятий.
         */
        private String startTime;

        /**
         * Время начала занятий.
         */
        private String endTime;

        /**
         * Список групп, к которым добавляются занятия по ids.
         */
        private List<Long> idGroups;

        /**
         * id преподавателя.
         */
        private Long idTeacher;


        static {
            FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        }

        /**
         *
         * @return Timestamp
         */
        @SneakyThrows
        public Timestamp getStartTime() {
            Date parsed = FORMAT.parse(startTime);
            return new Timestamp(parsed.getTime());
        }

        /**
         *
         * @return Timestamp
         */
        @SneakyThrows
        public Timestamp getEndTime() {
            Date parsed = FORMAT.parse(endTime);
            return new Timestamp(parsed.getTime());
        }

    }


}
