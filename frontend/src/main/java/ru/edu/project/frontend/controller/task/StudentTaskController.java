package ru.edu.project.frontend.controller.task;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;

import javax.validation.Valid;

@RequestMapping("/user/student/task/")
@Controller
public class StudentTaskController {

    /**
     *  Атрибут модели для заданий.
     */
    public static final String TASK = "task";

    /**
     * Атрибут модели для прогресса студента.
     */
    public static final String PROGRESS = "progress";

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Просмотр задания.
     *
     * @param id
     * @param model
     * @param auth
     * @return view
     */
    @GetMapping("/view/{id}")
    public String index(@PathVariable("id") final Long id, final Model model, final Authentication auth) {
        long userId = getUserId(auth);
        Task task = taskService.getTask(id);
        StudentProgress progress = taskService.getProgress(userId, task.getId());

        model.addAttribute(TASK, task);
        model.addAttribute(PROGRESS, progress);

        return "/task/student/view";
    }

    /**
     * Получаем форму с предварительной валидацией.
     *
     * @param form
     * @param model
     * @param auth
     * @return redirect url
     */
    @PostMapping("/create")
    public String createFormProcessing(
            @Valid
            @ModelAttribute final CreateForm form,
            final Model model,
            final Authentication auth
    ) {
        long userId = getUserId(auth);

        StudentProgress progress = taskService.createProgress(StudentProgress.builder()
                .userId(userId)
                .taskId(form.idTask)
                .response(form.response)
                .assessment(0)
                .verified(0)
                .build());

        return "redirect:/user/student/";
    }

    /**
     * Получаем userId.
     *
     * @param auth
     * @return userId
     */
    private long getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof UserDetailsId) {
            return ((UserDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }

    @Getter
    @Setter
    public static class CreateForm {

        /**
         * id задания.
         */
        private Long idTask;

        /**
         * Ответ.
         */
        private String response;

    }
}
