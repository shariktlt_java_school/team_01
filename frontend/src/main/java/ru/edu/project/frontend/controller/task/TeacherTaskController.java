package ru.edu.project.frontend.controller.task;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/user/teacher/task/")
@Controller
public class TeacherTaskController {

    /**
     * Атрибут модели для хранения задания.
     */
    public static final String TASK = "task";

    /**
     * Атрибут модели для хранения прогресса студента.
     */
    public static final String USER_PROGRESS = "userProgress";

    /**
     * Атрибут модели для хранения прогресса студента.
     */
    public static final String PROGRESS = "progress";

    /**
     * Ссылка на сервис пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Ссылка на сервис занятий.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Просмотр задания.
     *
     * @param id
     * @param model
     * @param auth
     * @return view
     */
    @GetMapping("/view/{id}")
    public String index(@PathVariable("id") final Long id, final Model model, final Authentication auth) {
        long userId = getUserId(auth);

        Task task = taskService.getTask(id);

        List<Long> tmpGroupId = new ArrayList<>();

        taskService.getLessonsIdByTaskId(id).forEach(aLong -> {
            lessonService.getGroupsIdsByLessonId(aLong).forEach(aLong1 -> {
                if (!tmpGroupId.contains(aLong1)) {
                    tmpGroupId.add(aLong1);
                }
            });
        });

        List<Long> tmpStudentId = new ArrayList<>();

        tmpGroupId.forEach(idGroup -> groupService.getStudentsByGroupId(idGroup).forEach(studentId -> {
            if (!tmpStudentId.contains(studentId)) {
                tmpStudentId.add(studentId);
            }
        }));

        List<User> users = userService.getUsers(tmpStudentId);

        Map<User, StudentProgress> userProgress = new HashMap<>();
        users.forEach(user -> {
            StudentProgress progress = taskService.getProgress(user.getId(), id);
            userProgress.put(user, progress);
        });

        model.addAttribute(TASK, task);
        model.addAttribute(USER_PROGRESS, userProgress);

        return "/task/teacher/view";
    }

    /**
     * @param idTask
     * @param idStudent
     * @return view
     */
    @GetMapping("/examination/{idTask}/{idStudent}")
    public ModelAndView examinationView(
            @PathVariable("idTask") final Long idTask,
            @PathVariable("idStudent") final Long idStudent
    ) {
        ModelAndView model = new ModelAndView("task/teacher/examination");

        Task task = taskService.getTask(idTask);
        StudentProgress progress = taskService.getProgress(idStudent, idTask);

        model.addObject(TASK, task);
        model.addObject(PROGRESS, progress);

        return model;
    }

    /**
     * Проставление оценки.
     *
     * @param idTask
     * @param idStudent
     * @param form
     * @return redirect
     */
    @PostMapping("/examination/{idTask}/{idStudent}")
    public String updateProgress(
            @PathVariable("idTask") final Long idTask,
            @PathVariable("idStudent") final Long idStudent,
            @ModelAttribute final CreateForm form) {

        taskService.updateProgress(StudentProgress.builder()
                        .userId(idStudent)
                        .taskId(idTask)
                        .verified(1)
                        .assessment(form.getAssessment())
                .build());
        return "redirect:/user/teacher/task/view/" + idTask;
    }

    @Getter
    @Setter
    public static class CreateForm {
        /**
         * Оценка.
         */
        @NotNull
        private int assessment;

    }

    /**
     * Получаем userId.
     *
     * @param auth
     * @return userId
     */
    private long getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof UserDetailsId) {
            return ((UserDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }
}
