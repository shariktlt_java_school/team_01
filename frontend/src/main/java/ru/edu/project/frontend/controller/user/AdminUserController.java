package ru.edu.project.frontend.controller.user;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserForm;
import ru.edu.project.backend.api.users.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RequestMapping("/user/administrator/")
@Controller
public class AdminUserController {

    /**
     * Атрибут модели для хранения списка ошибок.
     */
    public static final String FORM_ERROR_ATTR = "errorsList";

    /**
     * Атрибут модели для хранения студентов.
     */
    public static final String STUDENTS = "students";

    /**
     * Атрибут модели для хранения преподавателей.
     */
    public static final String TEACHERS = "teachers";

    /**
     * Атрибут модели для хранения преподавателей.
     */
    public static final String ADMINISTRATORS = "administrators";

    /**
     * Атрибут модели для хранения информации о пользователе.
     */
    public static final String USER = "user";

    /**
     * Атрибут модели для хранения информации о ролях.
     */
    public static final String ROLES = "roles";

    /**
     * Атрибут модели для хранения информации о группах.
     */
    public static final String GROUPS = "groups";

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис ролей.
     */
    @Autowired
    private RoleService roleService;

    /**
     * Ссылка на сервис пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Стартовая страница профиля администратора.
     *
     * @param model
     * @param auth
     * @return view
     */
    @GetMapping("/")
    public String index(final Model model, final Authentication auth) {
        long userId = getUserId(auth);

        model.addAttribute(USER, userService.getUser(userId));
        model.addAttribute(STUDENTS, userService.getUsers(roleService.getUsersByRole(EnumRoles.STUDENT)));
        model.addAttribute(TEACHERS, userService.getUsers(roleService.getUsersByRole(EnumRoles.TEACHER)));
        model.addAttribute(ADMINISTRATORS, userService.getUsers(roleService.getUsersByRole(EnumRoles.ADMINISTRATOR)));
        model.addAttribute(GROUPS, groupService.getAllGroups());

        return "/user/administrator/index";
    }

    /**
     * Отображение формы для создания пользователя.
     *
     * @param model
     * @return modelAndView
     */
    @GetMapping("/create")
    public String createFormUsers(final Model model) {
        model.addAttribute(ROLES, roleService.getRoles());
        return "/user/administrator/create";
    }

    /**
     * Получаем форму с предварительной валидацией.
     *
     * @param form
     * @param bindingResult результат валидации формы
     * @param model
     * @return redirect url
     */
    @PostMapping("/create")
    public String createFormProcessing(@Valid @ModelAttribute final CreateForm form,
                                       final BindingResult bindingResult,
                                       final Model model) {
        if (bindingResult.hasErrors()) {

            model.addAttribute(
                    FORM_ERROR_ATTR,
                    bindingResult.getAllErrors()
            );

            return createFormUsers(model);
        }

        User user = userService.createUser(UserForm.builder()
                        .firstName(form.getFirstName())
                        .lastName(form.getLastName())
                        .patronymic(form.getPatronymic())
                        .birthDay(form.getTime())
                        .login(form.getLogin())
                        .password(form.getPassword())
                        .role(form.getRole())
                .build());

        return "redirect:/user/administrator/";
    }

    /**
     * Получаем userId.
     *
     * @param auth
     * @return userId
     */
    private long getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof UserDetailsId) {
            return ((UserDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }

    @Getter
    @Setter
    public static class CreateForm {
        /**
         * Для парсинга даты.
         */
        private static final DateFormat FORMAT;

        static {
            FORMAT = new SimpleDateFormat("yyyy-MM-dd");
        }

        /**
         * login.
         */
        @NotNull
        private String login;

        /**
         * Имя.
         */
        private String firstName;

        /**
         * Фамилия.
         */
        private String lastName;

        /**
         * Отчество.
         */
        private String patronymic;

        /**
         * День рождения.
         */
        private String birthDay;

        /**
         * Пароль.
         */
        private String password;

        /**
         * Пароль2.
         */
        private String password2;

        /**
         * Роль.
         */
        private Long role;

        /**
         * Получение объекта календаря с временем посещения.
         *
         * @return календарь
         */
        @SneakyThrows
        public Timestamp getTime() {
            Date parsed = FORMAT.parse(birthDay);
            return new Timestamp(parsed.getTime());
        }
    }

}
