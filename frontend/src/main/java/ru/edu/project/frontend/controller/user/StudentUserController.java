package ru.edu.project.frontend.controller.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestMapping("/user/student/")
@Controller
public class StudentUserController {

    /**
     * Атрибут модели для хранения информации о пользователе.
     */
    public static final String USER = "user";

    /**
     * Атрибут модели для хранения информации о группах.
     */
    public static final String GROUPS = "groups";

    /**
     * Атрибут модели для хранения информации о занятиях.
     */
    public static final String LESSONS = "lessons";

    /**
     * Атрибут модели для хранения информации о занятиях и учителях.
     */
    public static final String LESSON_TEACHERS = "lessonTeachers";

    /**
     * Атрибут модели для хранения информации о сданных заданиях и оценках по ним.
     */
    public static final String STUDENT_PROGRESS = "studentProgress";

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Ссылка на сервис занятий.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Стартовая страница профиля студента.
     *
     * @param model
     * @param auth
     * @return view
     */
    @GetMapping("/")
    public String index(final Model model, final Authentication auth) {
        long userId = getUserId(auth);

        List<Group> groups = groupService.getGroupsByStudentId(userId);

        List<Lesson> lessons = new ArrayList<>();
        groups.forEach(group -> lessonService.getLessonsByGroupId(group.getId()).forEach(lesson -> lessons.add(lesson)));

        Map<Lesson, List<Task>> lessonTasks = getLessonListMap(lessons);

        Map<Lesson, List<User>> lessonTeachers = getTeachersByLesson(lessons);

        Map<Long, StudentProgress> progressByTaskId = getMapTaskIdStudentProgress(userId);

        model.addAttribute(USER, userService.getUser(userId));
        model.addAttribute(GROUPS, groups);
        model.addAttribute(LESSONS, lessonTasks);
        model.addAttribute(LESSON_TEACHERS, lessonTeachers);
        model.addAttribute(STUDENT_PROGRESS, progressByTaskId);

        return "/user/student/index";
    }

    /**
     * Получаем userId.
     *
     * @param auth
     * @return userId
     */
    private long getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof UserDetailsId) {
            return ((UserDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }

    /**
     * Формирует мапу занятий и список заданий к ним.
     *
     * @param lessons
     * @return map
     */
    private Map<Lesson, List<Task>> getLessonListMap(final List<Lesson> lessons) {
        if (lessons != null) {
            Map<Lesson, List<Task>> lessonTasks = new HashMap<>();
            lessons.forEach(lesson -> {
                lessonTasks.put(lesson, taskService.getTasksByLessonId(lesson.getId()));
            });
            lessonTasks.entrySet().stream().sorted((o1, o2) -> o2.getKey().getStartTime().compareTo(o1.getKey().getStartTime()));
            return lessonTasks;
        }
        return null;
    }

    /**
     *
     * @param lessons
     * @return map
     */
    private Map<Lesson, List<User>> getTeachersByLesson(final List<Lesson> lessons) {
        if (lessons != null) {
            Map<Lesson, List<User>> result = new HashMap<>();
            lessons.forEach(lesson -> {
                List<User> users = userService.getUsers(lessonService.getTeachersIdByLessonId(lesson.getId()));
                result.put(lesson, users);
            });
            return result;
        }
        return null;
    }

    /**
     * Формирует мапу id заданий и прогресс студента по этому заданию.
     *
     * @param userId
     * @return map
     */
    private Map<Long, StudentProgress> getMapTaskIdStudentProgress(final long userId) {
        List<StudentProgress> studentProgress = taskService.getProgressByStudentId(userId);
        Map<Long, StudentProgress> progressByTaskId = studentProgress.stream().collect(Collectors.toMap(StudentProgress::getTaskId, Function.identity()));
        return progressByTaskId;
    }
}
