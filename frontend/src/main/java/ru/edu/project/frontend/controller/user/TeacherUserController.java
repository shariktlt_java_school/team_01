package ru.edu.project.frontend.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RequestMapping("/user/teacher/")
@Controller
public class TeacherUserController {

    /**
     * Атрибут модели для хранения информации о пользователе.
     */
    public static final String USER = "user";

    /**
     * Атрибут модели для хранения информации о занятиях.
     */
    public static final String LESSONS = "lessons";

    /**
     * Атрибут модели для хранения информации о заданиях.
     */
    public static final String TASKS = "tasks";

    /**
     * Атрибут модели для хранения информации о занятиях и учителях.
     */
    public static final String LESSON_TEACHERS = "lessonTeachers";

    /**
     * Атрибут модели для хранения информации о занятиях и группах.
     */
    public static final String LESSON_GROUP = "lessonGroup";

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис занятий.
     */
    @Autowired
    private LessonService lessonService;

    /**
     * Ссылка на сервис пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Стартовая страница профиля преподавателя.
     *
     * @param model
     * @param auth
     * @return view
     */
    @GetMapping("/")
    public String index(final Model model, final Authentication auth) {
        long userId = getUserId(auth);

        List<Lesson> lessons = lessonService.getLessonsByUserId(userId);
        List<Long> lessonsId = lessons.stream().map(lesson -> lesson.getId()).collect(Collectors.toList());

        Map<Lesson, List<Group>> mapLessonGroups = getMalLessonGroups(lessonsId);

        List<Task> tasks = taskService.getTasksByLessonsId(lessonsId);

        Map<Lesson, List<User>> lessonTeachers = getTeachersByLesson(lessons);

        model.addAttribute(USER, userService.getUser(userId));
        model.addAttribute(LESSONS, lessons);
        model.addAttribute(LESSON_TEACHERS, lessonTeachers);
        model.addAttribute(TASKS, tasks);
        model.addAttribute(LESSON_GROUP, mapLessonGroups);

        return "/user/teacher/index";
    }

    /**
     *
     * @param lessonsId
     * @return map
     */
    private Map<Lesson, List<Group>> getMalLessonGroups(final List<Long> lessonsId) {
        Map<Lesson, List<Group>> result = new HashMap<>();
        Map<Long, List<Long>> tmp = new HashMap<>();
        lessonsId.forEach(
                id -> tmp.put(id, lessonService.getGroupsIdsByLessonId(id))
        );

        tmp.forEach((idLesson, groupsId) -> {
            Lesson lesson = lessonService.getLesson(idLesson);
            List<Group> tmpList = new ArrayList<>();
            groupsId.forEach(aLong -> tmpList.add(groupService.getGroup(aLong)));
            result.put(lesson, tmpList);
        });
        return result;
    }

    /**
     * Получаем userId.
     *
     * @param auth
     * @return userId
     */
    private long getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof UserDetailsId) {
            return ((UserDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }

    /**
     *
     * @param lessons
     * @return map
     */
    private Map<Lesson, List<User>> getTeachersByLesson(final List<Lesson> lessons) {
        if (lessons != null) {
            Map<Lesson, List<User>> result = new HashMap<>();
            lessons.forEach(lesson -> {
                List<User> users = userService.getUsers(lessonService.getTeachersIdByLessonId(lesson.getId()));
                result.put(lesson, users);
            });
            return result;
        }
        return null;
    }
}
