package ru.edu.project.app;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.edu.project.authorization.FrontendUserService;
import ru.edu.project.backend.api.users.UserForm;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class IndexControllerTest {

    public static final SimpleGrantedAuthority ANOTHER = new SimpleGrantedAuthority("ANOTHER");
    public static final SimpleGrantedAuthority STUDENT = new SimpleGrantedAuthority("STUDENT");
    public static final SimpleGrantedAuthority TEACHER = new SimpleGrantedAuthority("TEACHER");
    public static final SimpleGrantedAuthority ADMINISTRATOR = new SimpleGrantedAuthority("ADMINISTRATOR");

    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private FrontendUserService userService = new FrontendUserService();

    @InjectMocks
    private IndexController indexController;

    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    public void index() {
        Authentication authentication = mock(Authentication.class);
        List<SimpleGrantedAuthority> auth = new ArrayList<>();

        String view = indexController.index(null);
        assertEquals("index", view);

        when(authentication.isAuthenticated()).thenReturn(false);
        view = indexController.index(authentication);
        assertEquals("index", view);

        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getAuthorities())
                .thenAnswer(invocationOnMock -> auth);

        auth.add(STUDENT);
        view = indexController.index(authentication);
        assertEquals("redirect:/user/student/", view);

        auth.clear();
        auth.add(TEACHER);
        view = indexController.index(authentication);
        assertEquals("redirect:/user/teacher/", view);

        auth.clear();
        auth.add(ADMINISTRATOR);
        view = indexController.index(authentication);
        assertEquals("redirect:/user/administrator/", view);

        auth.clear();
        auth.add(ANOTHER);
        view = indexController.index(authentication);
        assertEquals("index", view);
    }

}