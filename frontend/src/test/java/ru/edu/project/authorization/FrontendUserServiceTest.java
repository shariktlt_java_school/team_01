package ru.edu.project.authorization;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class FrontendUserServiceTest {
    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserService userService;

    @Mock
    private RoleService roleService ;

    @InjectMocks
    private FrontendUserService frontendUserService;

    @Mock
    private User user;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    public void loadUserByUsername(){
        String login = "login";

        when(userService.loadUserByLogin(login)).thenReturn(user);
        when(user.getId()).thenReturn(1L);
    }

    @Test
    public void loadUserByIdNull(){
        String login = "login";
        UserService userService1 = mock(UserService.class);
        User user1 = mock(User.class);

        when(userService1.loadUserByLogin(login)).thenReturn(user1);
        when(user1.getId()).thenReturn(1L);
        NullPointerException thrown = Assertions.assertThrows(NullPointerException.class, () -> {
            UserDetails userDetails = frontendUserService.loadUserByUsername(login);
        });
    }

}