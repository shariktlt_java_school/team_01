package ru.edu.project.frontend.controller.group;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class AdminGroupControllerTest {
    @Mock
    private GroupService groupService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @InjectMocks
    private AdminGroupController groupController;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    public void createForm() {
        Model modelMock = mock(Model.class);
        List<User> users = new ArrayList<>();
        when(userService.getUsers(roleService.getUsersByRole(EnumRoles.STUDENT))).thenReturn(users);

        String view = groupController.createForm(modelMock);
        assertEquals("group/admin/create", view);
        verify(modelMock).addAttribute("students", users);
        verify(modelMock).addAttribute("path", "administrator");
    }

    @Test
    public void createFormProcessing() {
        AdminGroupController.CreateForm createForm = new AdminGroupController.CreateForm();
        List<Long> students = new ArrayList<>();
        students.add(1L);
        students.add(2L);
        createForm.setTitle("title");
        createForm.setStudents(students);

        Group group = Group.builder()
                .id(1L)
                .title("title").build();

        when(groupService.createGroup(any(GroupForm.class))).thenAnswer(invocationOnMock -> {
            GroupForm form = invocationOnMock.getArgument(0, GroupForm.class);
            assertEquals(createForm.getTitle(), form.getTitle());
            assertEquals(createForm.getStudents(), form.getStudents());
            return group;
        });

        String viewName = groupController.createFormProcessing(createForm, bindingResult, model);
        assertEquals("redirect:/user/administrator/", viewName);
        verify(groupService, times(1)).createGroup(any(GroupForm.class));

    }

    @Test
    public void createFormProcessingWithError() {
        AdminGroupController.CreateForm createForm = new AdminGroupController.CreateForm();
        List<Long> students = new ArrayList<>();
        students.add(1L);
        students.add(2L);
        createForm.setTitle("title");
        createForm.setStudents(students);

        when(bindingResult.hasErrors()).thenReturn(true);

        String viewName = groupController.createFormProcessing(createForm, bindingResult, model);
        assertEquals("group/admin/create", viewName);

    }

}