package ru.edu.project.frontend.controller.group;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class StudentGroupControllerTest {

    @Mock
    private GroupService groupService;

    @Mock
    private LessonService lessonService;

    @Mock
    private TaskService taskService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @InjectMocks
    private StudentGroupController studentGroupController;

    @Mock
    private Lesson lesson;

    @Mock
    private Authentication authentication;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void index() {
        Long id = 1L;

        Group group = mock(Group.class);
        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(lesson);

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(groupService.getGroup(id)).thenReturn(group);
        when(lessonService.getLessonsByGroupId(id)).thenReturn(lessonList);

        ModelAndView model = studentGroupController.index(id, authentication);

        assertEquals("group/student/view", model.getViewName());
    }

    @Test
    void indexNull() {
        Long id = 1L;

        Group group = mock(Group.class);
        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(lesson);

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(groupService.getGroup(id)).thenReturn(group);
        when(lessonService.getLessonsByGroupId(id)).thenReturn(null);

        ModelAndView model = studentGroupController.index(id, authentication);

        assertEquals("group/student/view", model.getViewName());
    }

    @Test
    void indexWithGroupNull() {
        Long id = 1L;
        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);

        ModelAndView model = studentGroupController.index(id, authentication);

        when(groupService.getGroup(id)).thenReturn(null);
        assertEquals("group/viewNotFound", model.getViewName());
        assertTrue(model.getStatus().is4xxClientError());
    }

    @Test
    void indexIllegalUser() {
        Long id = 1L;
        when(authentication.getPrincipal()).thenReturn(null);

        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> {
            ModelAndView model = studentGroupController.index(id, authentication);
        });
        Assertions.assertEquals("invalid auth.getPrincipal() object type", thrown.getMessage());
    }
}