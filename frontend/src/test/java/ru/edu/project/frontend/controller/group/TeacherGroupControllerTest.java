package ru.edu.project.frontend.controller.group;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.UserService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class TeacherGroupControllerTest {
    @Mock
    private GroupService groupService;

    @Mock
    private LessonService lessonService;

    @Mock
    private TaskService taskService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @InjectMocks
    private TeacherGroupController teacherGroupController;

    @Mock
    private Authentication authentication;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void index() {
        Long id = 1L;

        Group group = mock(Group.class);

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(groupService.getGroup(id)).thenReturn(group);

        ModelAndView model = teacherGroupController.index(id, authentication);

        assertEquals("group/teacher/view", model.getViewName());
    }

    @Test
    void indexNull() {
        Long id = 1L;

        Group group = mock(Group.class);

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(groupService.getGroup(id)).thenReturn(group);
        when(lessonService.getLessonsByGroupId(id)).thenReturn(null);

        ModelAndView model = teacherGroupController.index(id, authentication);

        assertEquals("group/teacher/view", model.getViewName());
    }

    @Test
    void indexWithGroupNull() {
        Long id = 1L;
        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);

        ModelAndView model = teacherGroupController.index(id, authentication);

        when(groupService.getGroup(id)).thenReturn(null);
        assertEquals("group/viewNotFound", model.getViewName());
        assertTrue(model.getStatus().is4xxClientError());
    }

    @Test
    void indexIllegalUser() {
        Long id = 1L;
        when(authentication.getPrincipal()).thenReturn(null);

        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> {
            ModelAndView model = teacherGroupController.index(id, authentication);
        });
        Assertions.assertEquals("invalid auth.getPrincipal() object type", thrown.getMessage());
    }
}