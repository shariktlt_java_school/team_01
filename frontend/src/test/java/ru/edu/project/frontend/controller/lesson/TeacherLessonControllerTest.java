package ru.edu.project.frontend.controller.lesson;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonForm;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;
import ru.edu.project.frontend.controller.task.TeacherTaskController;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.openMocks;

class TeacherLessonControllerTest {
    @Mock
    private GroupService groupService;

    @Mock
    private LessonService lessonService;

    @Mock
    private TaskService taskService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @InjectMocks
    private TeacherLessonController teacherTaskController;

    @Mock
    private Authentication authentication;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private Model model;

    @Mock
    private Task task;

    @Mock
    private User user;

    @Mock
    private Lesson lesson;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void createForm() {
        List<User> users = new ArrayList<>();
        List<Group> groups = new ArrayList<>();

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(groupService.getAllGroups()).thenReturn(groups);

        String view = teacherTaskController.createForm(model, authentication);

        assertEquals("lesson/create_les", view);
    }

    @Test
    void createFormProcessing() {
        TeacherLessonController.CreateForm createForm = new TeacherLessonController.CreateForm();
        createForm.setStartTime("2020-10-10T10:10");
        createForm.setEndTime("2020-10-10T10:10");

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);

        when(lessonService.createLesson(any(LessonForm.class))).thenAnswer(invocationOnMock -> {
            LessonForm form = invocationOnMock.getArgument(0, LessonForm.class);

            return lesson;
        });

        String viewName = teacherTaskController.createFormProcessing(createForm, bindingResult, model, authentication);
        assertEquals("redirect:/user/teacher/", viewName);
        verify(lessonService, times(1)).createLesson(any(LessonForm.class));
    }

    @Test
    void createFormProcessingError() {
        TeacherLessonController.CreateForm createForm = new TeacherLessonController.CreateForm();
        when(bindingResult.hasErrors()).thenReturn(true);
        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);

        String viewName = teacherTaskController.createFormProcessing(createForm, bindingResult, model, authentication);
        assertEquals("lesson/create_les", viewName);
    }
}