package ru.edu.project.frontend.controller.task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.UserService;
import ru.edu.project.frontend.controller.group.TeacherGroupController;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class StudentTaskControllerTest {
    @Mock
    private GroupService groupService;

    @Mock
    private LessonService lessonService;

    @Mock
    private TaskService taskService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @Mock
    private Model model;

    @Mock
    private StudentProgress studentProgress;

    @Mock
    private Task task;

    @Mock
    private Authentication authentication;

    @InjectMocks
    private StudentTaskController studentTaskController;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    public void index() {
        Long id = 1L;

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(taskService.getTask(id)).thenReturn(task);
        when(task.getId()).thenReturn(id);

        String view = studentTaskController.index(id, model, authentication);

        assertEquals("/task/student/view", view);
    }

    @Test
    public void createFormProcessing() {
        StudentTaskController.CreateForm createForm = new StudentTaskController.CreateForm();
        createForm.setIdTask(1L);
        createForm.setResponse("response");

        StudentProgress studentProgress = StudentProgress.builder().build();
        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(taskService.createProgress(any(StudentProgress.class))).thenAnswer(invocationOnMock -> {
            StudentProgress form = invocationOnMock.getArgument(0, StudentProgress.class);
            assertEquals(createForm.getIdTask(), form.getTaskId());
            assertEquals(createForm.getResponse(), form.getResponse());
            return studentProgress;
        });

        String viewName = studentTaskController.createFormProcessing(createForm, model, authentication);
        assertEquals("redirect:/user/student/", viewName);
    }

    @Test
    public void createFormProcessingIllegalUser() {
        StudentTaskController.CreateForm createForm = new StudentTaskController.CreateForm();

        when(authentication.getPrincipal()).thenReturn(null);

        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> {
            String viewName = studentTaskController.createFormProcessing(createForm, model, authentication);
        });
        Assertions.assertEquals("invalid auth.getPrincipal() object type", thrown.getMessage());
    }
}