package ru.edu.project.frontend.controller.task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.StudentProgress;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;
import ru.edu.project.frontend.controller.group.StudentGroupController;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class TeacherTaskControllerTest {
    @Mock
    private GroupService groupService;

    @Mock
    private LessonService lessonService;

    @Mock
    private TaskService taskService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @InjectMocks
    private TeacherTaskController teacherTaskController;

    @Mock
    private Authentication authentication;

    @Mock
    private Model model;

    @Mock
    private Task task;

    @Mock
    private User user;

    @Mock
    private Lesson lesson;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void index() {
        Long id = 1L;
        List<Long> lessonsByTaskId = new ArrayList<>();
        lessonsByTaskId.add(1L);
        lessonsByTaskId.add(2L);
        lessonsByTaskId.add(2L);

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);

        when(taskService.getLessonsIdByTaskId(id)).thenReturn(lessonsByTaskId);
        when(lessonService.getGroupsIdsByLessonId(id)).thenReturn(lessonsByTaskId);
        when(groupService.getStudentsByGroupId(id)).thenReturn(lessonsByTaskId);

        String view = teacherTaskController.index(id, model, authentication);

        assertEquals("/task/teacher/view", view);
    }
    @Test
    void indexIllegalUser() {
        Long id = 1L;

        when(authentication.getPrincipal()).thenReturn(null);
        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> {
            String view = teacherTaskController.index(id, model, authentication);
        });
        Assertions.assertEquals("invalid auth.getPrincipal() object type", thrown.getMessage());

    }

    @Test
    void examinationView() {
        Long idTask = 1L;
        Long idStudent = 1L;

        ModelAndView model = teacherTaskController.examinationView(idTask, idStudent);
        assertEquals("task/teacher/examination", model.getViewName());
    }

    @Test
    void updateProgress() {
        TeacherTaskController.CreateForm createForm = new TeacherTaskController.CreateForm();
        createForm.setAssessment(5);

        StudentProgress studentProgress = StudentProgress.builder()
                .userId(1L)
                .taskId(1L)
                .response("response")
                .assessment(0)
                .verified(0)
                .build();

        when(taskService.updateProgress(any(StudentProgress.class))).thenAnswer(invocationOnMock -> {
            StudentProgress st = invocationOnMock.getArgument(0, StudentProgress.class);
            assertEquals(st.getAssessment(), st.getAssessment());
            return studentProgress;
        });

        String viewName = teacherTaskController.updateProgress(1L, 1L, createForm);
        assertEquals("redirect:/user/teacher/task/view/1", viewName);
    }
}