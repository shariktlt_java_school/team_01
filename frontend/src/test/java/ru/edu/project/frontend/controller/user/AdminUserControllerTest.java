package ru.edu.project.frontend.controller.user;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.EnumRoles;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserForm;
import ru.edu.project.backend.api.users.UserService;
import ru.edu.project.frontend.controller.task.TeacherTaskController;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class AdminUserControllerTest {
    @Mock
    private GroupService groupService;

    @Mock
    private LessonService lessonService;

    @Mock
    private TaskService taskService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @InjectMocks
    private AdminUserController adminUserController;

    @Mock
    private Authentication authentication;

    @Mock
    private Model model;

    @Mock
    private Task task;

    @Mock
    private User user;

    @Mock
    private Lesson lesson;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private BindingResult bindingResult;

    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void index() {
        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);

        String view = adminUserController.index(model, authentication);

        assertEquals("/user/administrator/index", view);
    }

    @Test
    void indexIllegalUser() {
        when(authentication.getPrincipal()).thenReturn(null);
        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> {
            String view = adminUserController.index(model, authentication);
        });
        Assertions.assertEquals("invalid auth.getPrincipal() object type", thrown.getMessage());

    }

    @Test
    void createFormUsers() {
        String view = adminUserController.createFormUsers(model);

        assertEquals("/user/administrator/create", view);
    }

    @Test
    @SneakyThrows
    void createFormProcessing() {
        AdminUserController.CreateForm createForm = new AdminUserController.CreateForm();

        String login = "login";
        String firstName = "firstName";
        String lastName = "lastname";
        String patronymic = "patronymic";
        String birthDay = "2000-10-10";
        String password = "password";
        Long role = 1L;

        Date parsed = FORMAT.parse(birthDay);
        Timestamp birthDayRes = new Timestamp(parsed.getTime());
        
        createForm.setFirstName(firstName);
        createForm.setLastName(lastName);
        createForm.setPatronymic(patronymic);
        createForm.setBirthDay("2000-10-10");
        createForm.setLogin(login);
        createForm.setPassword(password);
        createForm.setPassword2(password);
        createForm.setRole(role);

        User user = User.builder()
                .id(1L)
                .firstName(firstName)
                .lastName(lastName)
                .patronymic(patronymic)
                .birthday(birthDayRes)
                .login(login)
                .password(password)
                .build();

        when(passwordEncoder.encode(password)).thenReturn(password);
        when(userService.createUser(any(UserForm.class))).thenAnswer(invocationOnMock -> {
            UserForm info = invocationOnMock.getArgument(0, UserForm.class);
            assertEquals(login, info.getLogin());
            assertEquals(firstName, info.getFirstName());
            assertEquals(lastName, info.getLastName());
            assertEquals(patronymic, info.getPatronymic());
            assertEquals(birthDayRes, info.getBirthDay());
            assertEquals(password, info.getPassword());
            assertEquals(role, info.getRole());

            return user;
        });

        String view = adminUserController.createFormProcessing(createForm, bindingResult, model);

        verify(userService, times(1)).createUser(any(UserForm.class));
        assertEquals("redirect:/user/administrator/", view);
    }

    @Test
    void createFormProcessingHasErrors() {
        AdminUserController.CreateForm createForm = new AdminUserController.CreateForm();
        when(bindingResult.hasErrors()).thenReturn(true);

        String view = adminUserController.createFormProcessing(createForm, bindingResult, model);

        assertEquals("/user/administrator/create", view);

    }
}