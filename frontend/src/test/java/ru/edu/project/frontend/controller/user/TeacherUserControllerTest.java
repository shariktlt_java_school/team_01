package ru.edu.project.frontend.controller.user;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.groups.Group;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonService;
import ru.edu.project.backend.api.roles.RoleService;
import ru.edu.project.backend.api.tasks.Task;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.api.users.UserService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.any;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class TeacherUserControllerTest {
    @Mock
    private GroupService groupService;

    @Mock
    private LessonService lessonService;

    @Mock
    private TaskService taskService;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailsId userDetailsIdMock;

    @InjectMocks
    private TeacherUserController teacherUserController;

    @Mock
    private Authentication authentication;

    @Mock
    private Model model;

    @Mock
    private Group group;

    @Mock
    private Task task;

    @Mock
    private User user;

    @Mock
    private Lesson lesson;

    @Mock
    private BindingResult bindingResult;

    private static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void index() {
        List<Long> longList = new ArrayList<>();
        longList.add(1L);

        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(Lesson.builder().id(1L).build());

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(lessonService.getGroupsIdsByLessonId(userDetailsIdMock.getUserId())).thenReturn(longList);
        when(lessonService.getLessonsByUserId(userDetailsIdMock.getUserId())).thenReturn(lessonList);

        String view = teacherUserController.index(model, authentication);

        assertEquals("/user/teacher/index", view);
    }
    @Test
    void indexNullLessons() {
        List<Long> longList = new ArrayList<>();
        longList.add(1L);

        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(Lesson.builder().id(1L).build());

        when(authentication.getPrincipal()).thenReturn(userDetailsIdMock);
        when(lessonService.getGroupsIdsByLessonId(userDetailsIdMock.getUserId())).thenReturn(null);

        String view = teacherUserController.index(model, authentication);

    }

    @Test
    void indexIllegalUser() {
        when(authentication.getPrincipal()).thenReturn(null);
        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> {
            String view = teacherUserController.index(model, authentication);
        });
        Assertions.assertEquals("invalid auth.getPrincipal() object type", thrown.getMessage());

    }
}